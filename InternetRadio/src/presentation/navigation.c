/*
 * navigation.c
 *
 * Created: 19-2-2019 12:10:24
 *  Author: dvisser4
 */ 

#define LOG_MODULE  LOG_MAIN_MODULE

#include <stdio.h>
#include "presentation/navigation.h"
#include "presentation/menu.h"

#include "hardware/keyboard.h"

#include "logic/log.h"
#include "logic/keyboardLogic.h"

void navigaton_Control(int, int*);

/*
 * checks what needs to be done when the user presses a button
 *
 * param int
 *
 * return void
 *
 */
void navigation_HandleKeypress(int key)
{
	int *a = 0;
	int i = 0;
	switch (key)
	{
		case KEY_SPEC:
			break;
		case KEY_ESC:
			if (menu[currentItem].OnKey[key] != NULL)
			{
				(*menu[currentItem].OnKey[key])();
			}
			break;
		case KEY_ALT:
			resetVariables();
			i = 1;
			navigaton_Control(key, &a);
			break;	
		case KEY_01:
		case KEY_02:
		case KEY_03:
		case KEY_04:
		case KEY_05:	
		case KEY_UP:
		case KEY_OK:
		case KEY_DOWN:
		case KEY_RIGHT:
		case KEY_LEFT:
			i = 1;
			navigaton_Control(key, &a);
			break;
		case KEY_POWER:
			(*menu[currentItem].OnKey[key])();
			break;
		case KEY_SETUP:
			break;
	}
		
	if(i == 1)
	{
		currentItem = 0;
		
		while (menu[currentItem].id != currentId)
		{
			currentItem += 1;
		}

		if (menu[currentItem].OnEntry != NULL && a != 0)
		{
			(*menu[currentItem].OnEntry)();
		}
	}
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void navigation_initMenu(void)
{
	currentItem = 0;
	currentId = menu[currentItem].id;
	(*menu[currentItem].OnEntry)();
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void navigaton_Control(int key, int *a)
{
	if (menu[currentItem].OnKey[key] != NULL)
	{
		(*menu[currentItem].OnKey[key])();
	}
	if(menu[currentItem].nextId[key] != CURRENT_MENU)
	{
		if((menu[currentItem].OnExit) != NULL)
		{
			(*menu[currentItem].OnExit)();
		}			
		currentId = menu[currentItem].nextId[key];
		*a = 1;
	}
}
