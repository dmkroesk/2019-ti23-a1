/*
 * menu.c
 *
 * Created: 19-2-2019 11:57:03
 *  Author: dvisser4
 */ 

#define LOG_MODULE  LOG_MAIN_MODULE

#include <stdio.h>
#include "presentation/menu.h"
#include "presentation/navigation.h"
#include "logic/keyboardLogic.h"
#include "logic/log.h"
#include "logic/alarmLogic.h"
#include "logic/flash.h"
#include "logic/settings.h"
#include "logic/lcdLogic.h"
#include "logic/rtcLogic.h"

#include "hardware/lcd.h"
#include "hardware/ethernet.h"
#include "hardware/lcd.h"

void menu_onEntryMain(void);
void menu_onEntryMenu(void);
void menu_onEntryFavorite(void);
void menu_onEntryRadio(void);
void menu_onEntryAlarm(void);
void menu_onEntrySettings(void);
void menu_onEntryTime(void);
void menu_onEntryAlarmAdd(void);
void menu_onEntryAlarmChange(void);
void menu_onEntryAlarmDelete(void);
void menu_onEntryStopAlarmTime(void);
void menu_onEntryMAC(void);
void menu_onEntryAudio(void);
void menu_onEntryBassSettings(void);
void menu_onEntryTrebleSettings(void);

void menu_onExitMain(void);
void menu_onExitMenu(void);
void menu_onExitFavorite(void);
void menu_onExitRadio(void);
void menu_onExitAlarm(void);
void menu_onExitSettings(void);
void menu_onExitTime(void);
void menu_onExitAlarmAdd(void);
void menu_onExitAlarmChange(void);
void menu_onExitAlarmDelete(void);
void menu_onExitStopAlarmTime(void);
void menu_onExitMAC(void);
void menu_onExitAudioSetting(void);
void menu_onExitBass(void);
void menu_onExitTreble(void);
void menu_onEntryInfrared(void);
void menu_onExitInfrared(void);

Settings_t *settings;
char minutes[7];
char newsTitle[150];

/*
 * creates the whole menu structure
 *
 * param void
 *
 * return void
 *
 */
MenuItem_t menu[] = 
{
	{
		MENU_MAIN,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, CURRENT_MENU, CURRENT_MENU, 
			CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_Up, 
			pressed_Ok, audio_volDown, pressed_Down, audio_volUp, pressed_power, NULL },
		menu_onEntryMain,
		menu_onExitMain
	},
	{
		MENU_MENU,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_MAIN, CURRENT_MENU,
			CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ "Settings", "Audio","Alarm", "Favorites", "Radio", NULL},
		{ MENU_SETTINGS, MENU_AUDIO, MENU_ALARM, MENU_FAVORITES, MENU_RADIO, NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_Up,
			pressed_Ok, audio_volDown, pressed_Down, audio_volUp, pressed_power, NULL },
		menu_onEntryMenu, 
		menu_onExitMenu 
	},
	
	{
		MENU_AUDIO,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_MENU, CURRENT_MENU,
		CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ "Bass", "Treble", NULL },
		{ MENU_AUDIO_BASS, MENU_AUDIO_TREBLE, NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_Up,
		pressed_Ok, audio_volDown, pressed_Down, audio_volUp, NULL, NULL },
		menu_onEntryAudio,
		menu_onExitAudioSetting
	},
	{
		MENU_FAVORITES,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_MENU, CURRENT_MENU,
			CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ "Favoriet 1", "Favoriet 2", "Favoriet 3", "Favoriet 4", "Favoriet 5", NULL },
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_Up,
			pressed_Ok, favorites_pressed_left, pressed_Down, favorites_pressed_right, pressed_power, NULL },
		menu_onEntryFavorite,
		menu_onExitFavorite
	},
	{
		MENU_RADIO,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_MENU, CURRENT_MENU,
			CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_Up,
			pressed_Ok, pressed_Left, pressed_Down, pressed_Right, pressed_power, NULL },
		menu_onEntryRadio, 
		menu_onExitRadio 
	},
	{
		MENU_ALARM,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_MENU, CURRENT_MENU,
			CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ "Add alarms",	"Delete alarms"},
		{ MENU_ALARM_ADD, MENU_ALARM_DELETE},
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_Up,
			pressed_Ok, pressed_Left, pressed_Down, pressed_Right, pressed_power, NULL },
		menu_onEntryAlarm,
		menu_onExitAlarm
	},
	{
		MENU_SETTINGS,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_MENU, CURRENT_MENU,
			CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ "Time", "Alarm time", "Change mac", "Infrared" },
		{ MENU_SETTINGS_TIME, MENU_SETTINGS_STOPALARMTIME,  MENU_SETTINGS_MAC, MENU_SETTINGS_INFRARED },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_Up,
			pressed_Ok, pressed_Left, pressed_Down, pressed_Right, pressed_power, NULL },
		menu_onEntrySettings,
		menu_onExitSettings
	},
	{
		MENU_SETTINGS_STOPALARMTIME,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_SETTINGS, CURRENT_MENU,
			CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_increment_alarmTime,
			pressed_alarmstop_accept, pressed_next, pressed_decrement_alarmTime, pressed_prev, pressed_power, NULL },
		menu_onEntryStopAlarmTime,
		menu_onExitStopAlarmTime
	},
		{
		MENU_SETTINGS_TIME,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_SETTINGS, CURRENT_MENU,
			CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_increment,
		pressed_time_accept, pressed_next, pressed_decrement, pressed_prev, pressed_power, NULL },
		menu_onEntryTime,
		menu_onExitTime
	},
		{
		MENU_SETTINGS_MAC,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_SETTINGS, CURRENT_MENU,
			CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, MAC_increment,
		MAC_pressed_ok, MAC_pressed_prev, MAC_decrement, MAC_pressed_next, pressed_power, NULL },
		menu_onEntryMAC,
		menu_onExitMAC
	},
	{
		MENU_SETTINGS_INFRARED,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_SETTINGS, CURRENT_MENU,
		CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_Up,
		infrared_Pressed_Ok, infrared_Pressed_Left_Or_Right, pressed_Down, infrared_Pressed_Left_Or_Right, pressed_power, NULL },
		menu_onEntryInfrared,
		menu_onExitInfrared
	},
	{
		MENU_ALARM_ADD,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_ALARM, CURRENT_MENU,
		CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ "00:00", "On", "Sound", NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_increment_alarm,
		pressed_accept_alarm, pressed_next_alarm, pressed_decrement_alarm, pressed_prev_alarm, pressed_power, NULL },
		menu_onEntryAlarmAdd,
		menu_onExitAlarmAdd
	},
	{
		MENU_ALARM_CHANGE,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_ALARM, CURRENT_MENU,
		CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, pressed_Up,
		pressed_Ok, pressed_Left, pressed_Down, pressed_Right, pressed_power, NULL },
		menu_onEntryAlarmChange,
		menu_onExitAlarmChange
	},
	{
		MENU_ALARM_DELETE,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_ALARM, CURRENT_MENU,
		CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_Esc, kbl_keyPressedUpAlarmDelete,
		kbl_deleteAlarm, pressed_Left, kbl_keyPressedDownAlarmDelete, pressed_Right, pressed_power, NULL },
		menu_onEntryAlarmDelete,
		menu_onExitAlarmDelete
	},
	{
		MENU_AUDIO_BASS,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_AUDIO, CURRENT_MENU,
		CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_bass_Ok, pressed_bass_Up,
		pressed_bass_Ok, pressed_Left, pressed_bass_Down, pressed_Right, NULL, NULL },
		menu_onEntryBassSettings,
		menu_onExitBass
	},
	{
		MENU_AUDIO_TREBLE,
		{ CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, MENU_MENU, MENU_AUDIO, CURRENT_MENU,
		CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU, CURRENT_MENU },
		{ NULL },
		{ NULL },
		{ NULL, pressed_1, pressed_2, pressed_3, pressed_4, pressed_5, NULL, pressed_treble_Ok, pressed_treble_Up,
		pressed_treble_Ok, pressed_Left, pressed_treble_Down, pressed_Right, NULL, NULL },
		menu_onEntryTrebleSettings,
		menu_onExitTreble
	}
};

void menu_onEntryMain(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	rtc_update();

	// ethernet_getRequest("167.114.118.40", 80, "/v2/everything?pageSize=1&q=Apple&from=2019-03-21&sortBy=popularity&apiKey=40b1cae7c83d4441841b524eb9b069b8", "newsapi.org");
    // getTitle(&newsTitle);
	// lcd_writeStringSecond(newsTitle);
	// int i = 0;
	// for(i ; i < 16 ; i++){
	// 	LcdChar(newsTitle[i]);
	// }

	LogMsg_P(LOG_INFO, PSTR("Entered menu Main"));
}
void menu_onEntryMenu(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	rtc_update();
	lcd_writeStringSecond(menu[currentItem].selectedItem[currentSelectedItem]);
	lcd_custom_buttons();
	LogMsg_P(LOG_INFO, PSTR("Entered menu Menu"));
}
void menu_onEntryFavorite(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Favorites");
	LogMsg_P(LOG_INFO, PSTR("Entered menu Favorite"));
}
void menu_onEntryRadio(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Radios");
	LogMsg_P(LOG_INFO, PSTR("Entered menu Radio"));
}
void menu_onEntryAlarm(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	rtc_update();
	lcd_writeStringSecond(menu[currentItem].selectedItem[currentSelectedItem]);
	alarm_savePeriodicAlarmGlobally();
	LogMsg_P(LOG_INFO, PSTR("Entered menu Alarm"));
}
void menu_onEntrySettings(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Settings");
	lcd_writeStringSecond(menu[currentItem].selectedItem[currentSelectedItem]);
	LogMsg_P(LOG_INFO, PSTR("Entered menu Settings"));
}
void menu_onEntryTime(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("00:00 00-00-00");
	LogMsg_P(LOG_INFO, PSTR("Entered menu Time"));
}
void menu_onEntryAlarmAdd(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Add alarm");
	lcd_writeStringSecond(menu[currentItem].selectedItem[currentSelectedItem]);
	LogMsg_P(LOG_INFO, PSTR("Entered menu Alarm Add"));
}
void menu_onEntryAlarmChange(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Change alarms");
	LogMsg_P(LOG_INFO, PSTR("Entered menu Alarm Change"));
}

void menu_onEntryAlarmDelete(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Delete alarms");
	kbl_showAlarms(0);
	LogMsg_P(LOG_INFO, PSTR("Entered menu Alarm Delete"));
}

void menu_onEntryMAC(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Change MAC");
	char *a = flash_readMacAddress();
	lcd_writeStringSecond(a);
	LogMsg_P(LOG_INFO, PSTR("Entered menu MAC change"));
}

void menu_onExitMAC(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited change mac address"));
}

void menu_onEntryInfrared(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Infrared");
	lcd_writeStringSecond("Off");
	LogMsg_P(LOG_INFO, PSTR("Entered menu settings infrared"));
}

void menu_onExitInfrared(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited settings infrared"));
}

void menu_onExitStopAlarmTime(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited stop alarm time"));
}

void menu_onEntryStopAlarmTime(void)
{
	settings = flash_readSettings(SETTINGS_DATA_PAGE);
	sprintf(minutes, "%02d minutes", settings->stopalarmminutes); 
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Enter stop time");
	lcd_writeStringSecond(minutes);
	LogMsg_P(LOG_INFO, PSTR("Entered stop Time %d"), settings->stopalarmminutes); 
}

void menu_onEntryBassSettings(void)
{
	lcd_clearDisplay();
	lcd_writeString("Audio Settings");
	char* bassString[9];
	sprintf(bassString, "Bass: %d", audio_getBass());
	lcd_writeStringSecond(bassString);
}

void menu_onEntryTrebleSettings(void)
{	
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Audio Settings");
	char* trebleString[11];
	sprintf(trebleString, "Treble: %d", audio_getTreble() - 8);
	lcd_writeStringSecond(trebleString);
}

void menu_onEntryAudio(void)
{
	currentSelectedItem = 0;
	lcd_clearDisplay();
	lcd_writeString("Audio Settings");
	lcd_writeStringSecond(menu[currentItem].selectedItem[currentSelectedItem]);
	lcd_custom_buttons();
}

// void menu_onExitMAC(void)
// {
// 	LogMsg_P(LOG_INFO, PSTR("Exited change mac address"));
// }

// void menu_onExitStopAlarmTime(void)
// {
// 	LogMsg_P(LOG_INFO, PSTR("Exited stop alarm time"));
// }

void menu_onExitMain(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Main"));
}
void menu_onExitMenu(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Menu"));
}
void menu_onExitFavorite(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Favorite"));
}
void menu_onExitRadio(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Radio"));
}
void menu_onExitAlarm(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Alarm"));
}
void menu_onExitSettings(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Settings"));
}
void menu_onExitTime(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Time"));
}
void menu_onExitAlarmAdd(void)
{
	alarm_savePeriodicAlarmGlobally();
	LogMsg_P(LOG_INFO, PSTR("Exited menu Alarm Add"));
}
void menu_onExitAlarmChange(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Alarm Change"));
}
void menu_onExitAlarmDelete(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Alarm Delete"));
}

void menu_onExitAudioSetting(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Audio setting"));
}

void menu_onExitTreble(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Treble and saved treb value"));
}

void menu_onExitBass(void)
{
	LogMsg_P(LOG_INFO, PSTR("Exited menu Bass and saved bass value"));
}
