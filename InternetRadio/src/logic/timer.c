/*
 * timer.c
 *
 * Created: 19/02/2019 11:04:31
 *  Author: Tim Herreijgers
 */ 

#include "logic/timer.h"
#include <sys/timer.h>

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void *timer_callAfter(int msec, timerCallback_t callback, void *args)
{
	return NutTimerStart(msec, callback, args, 0);
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void timer_stop(handle_t handle)
{
	NutTimerStop(handle);
}
