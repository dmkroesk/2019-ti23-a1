#include <stdio.h>
#include <stdbool.h>

#include "logic/lcdBacklightTimeout.h"
#include "logic/lcdLogic.h"
#include "logic/timer.h"
#include "logic/keyboardLogic.h"

#define TIMEOUT 10000

static handle_t timerHandle;
static bool enabled;

/**
 * Private timer callback function
 * 
 * @param timer The timer handle
 * @param args The arguments
 */
void lbt_timerCallback(handle_t timer, void *args);

/**
 * Private button callback function
 * 
 * @param button The pressed button
 */
void lbt_buttonCallback(int button);

/*
 * 
 *
 * param none
 *
 * return void
 *
 */
void lbt_init(void)
{
    timerHandle = 0x00;
    enabled = true;
    kbl_addCallback(lbt_buttonCallback);
}

/*
 * 
 *
 * param none
 *
 * return void
 *
 */
void lbt_disable(void)
{
    enabled = false;
}

/*
 * 
 *
 * param none
 *
 * return void
 *
 */
void lbt_enable(void)
{
    enabled = true;
}

/*
 * 
 *
 * param none
 *
 * return void
 *
 */
void lbt_timerCallback(handle_t timer, void *args)
{
    timer_stop(timer);
    timerHandle = NULL;
    lcd_backlightOff();
}

/*
 * 
 *
 * param none
 *
 * return void
 *
 */
void lbt_buttonCallback(int button)
{
    if(!enabled) 
        return;

    if(timerHandle != NULL)
        timer_stop(timerHandle);

    timerHandle = timer_callAfter(TIMEOUT, lbt_timerCallback, NULL);
    lcd_backlightOn();
}
