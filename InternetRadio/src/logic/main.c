/*
 * main.c
 *
 * Created: 14/02/2019 11:41:46
 *  Author: Tim Herreijgers
 */

#define LOG_MODULE  LOG_MAIN_MODULE

#define RTC_COUNTER_PRESCALER 1000

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <avr/io.h>

#include <time.h>
#include <stdio.h>
#include <string.h>

#include "hardware/system.h"
#include "hardware/uart0driver.h"
#include "logic/rtcLogic.h"
#include "hardware/keyboard.h"
#include "hardware/portio.h"
#include "logic/log.h"
#include "presentation/menu.h"
#include "presentation/navigation.h"
#include "logic/keyboardLogic.h"
#include "logic/lcdLogic.h"
#include "logic/lcdBacklightTimeout.h"
#include "logic/timer.h"
#include "hardware/lcd.h"
#include "logic/flash.h"
#include "logic/settings.h"
#include "logic/alarmLogic.h"
#include "logic/audio.h"
#include "hardware/ethernet.h"
#include "hardware/infrared.h"
#include "logic/sdcard.h"

int rtcCounter = 0;

char newsTitle[50];

static void SysMainBeatInterrupt(void*);
static void SysControlMainBeat(u_char);
void SysInitIO(void);

tm timetest;

static void SysMainBeatInterrupt(void *p)
{
	KbScan();
	sd_checkForCard();
}

/*!
 * \brief Initialise Digital IO
 *  init inputs to '0', outputs to '1' (DDRxn='0' or '1')
 *
 *  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
 *  is written to the pin (PORTxn='1')
 */
/* */
void SysInitIO(void)
{
    /*
     *  Port B:     VS1011, MMC CS/WP, SPI
     *  output:     all, except b3 (SPI Master In)
     *  input:      SPI Master In
     *  pull-up:    none
     */
    outp(0xF7, DDRB);

    /*
     *  Port C:     Address bus
     */

    /*
     *  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
     *  output:     Keyboard colums 2 & 3
     *  input:      LCD_data, SDA, SCL (TWI)
     *  pull-up:    LCD_data, SDA & SCL
     */
    outp(0x0C, DDRD);
    outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

    /*
     *  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
     *  output:     CS Flash, LCD BL/Enable, USB Tx
     *  input:      VS1011 (DREQ), RTL8019, IR
     *  pull-up:    USB Rx
     */
    outp(0x8E, DDRE);
    outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

    /*
     *  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
     *  output:     LCD RS/RW, LED
     *  input:      Keyboard_Rows, MCC-detect
     *  pull-up:    Keyboard_Rows, MCC-detect
     *  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
     */
#ifndef USE_JTAG
    sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
    sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256

	
#endif //USE_JTAG

	cbi(OCDR, IDRD);
	cbi(OCDR, IDRD);


    outp(0x0E, DDRF);
    outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

    /*
     *  Port G:     Keyboard_cols, Bus_control
     *  output:     Keyboard_cols
     *  input:      Bus Control (internal control)
     *  pull-up:    none
     */
    outp(0x18, DDRG);
}

/* Global varibales */

void initialMemoryInit( void)
{
	AlarmTable_t *table = malloc(sizeof(AlarmTable_t));
	table->alarm1Active = false;
	table->alarm2Active = false;
	table->alarm3Active = false;
	table->alarm4Active = false;
	table->alarm5Active = false;
	flash_writeAlarmTable(table);
	char macAddress[13] = {'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '\0'};	
	flash_writeMACAddress(macAddress);
	int i = 1;
	for(i = 1; i < 6; i++)
	{
		flash_writeFavorite(i, 0);
		LogMsg_P(LOG_INFO, PSTR("-------------------------------------------"));
	}
	flash_writeInteger(0);
	free(table);
}

static void SysControlMainBeat(u_char OnOff)
{
	int nError = 0;

	if (OnOff==ON)
	{
		nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
		if (nError == 0)
		{
			init_8_bit_timer();
		}
	}
	else
	{
		// disable overflow interrupt
		disable_8_bit_timer_ovfl_int();
	}
}

int main(void)
{	  
	
	SysInitIO();
    SysControlMainBeat(ON);  
	
	Uart0DriverInit();
	Uart0DriverStart();
	LogInit();
	
    // Init LCD display
    lcd_init();
    lcd_backlightOn();
    lcd_clearDisplay();
    lcd_writeString("Booting radio");

	//starts uart driver for log debugging	
	
	printf("start");
	rtc_init();
    flash_init();
	KbInit();
	
	//init log so logging is possible
		
	IRInit();
	
	lbt_init();
	alarm_init();
	kbl_init();	
    audio_init();
	
	ethernet_init();
	sd_init();

	sei();

    LogMsg_P(LOG_DEBUG, PSTR("Check connection result: %d"), ethernet_checkconnection());
	
    LogMsg_P(LOG_DEBUG, PSTR("Done booting"));

	int a = 0;
	while(1)
	{
		kbl_update();
		
		if(getInfraredChanged() == 1)
		{
			flash_readInteger(&a);
			LogMsg_P(LOG_INFO, PSTR("Infrared changed to: %d"), a);
			resetInfrared();
		}

		if(getSentNewKey() == 1 && a == 1)
		{
			navigation_HandleKeypress(getLastButton());
			printf("Last button: %d", getLastButton());
			resetSentNewKey();
			lbt_buttonCallback(getLastButton());
		}
		
		NutSleep(50);
	}
	return 0;
}
