/*
 * radio.c
 *
 * Created: 14-3-2019 12:36:37
 *  Author: olaf
 */ 

#include "logic/radio.h"

RadioStation_t radioStationList[RADIO_STATION_LIST_SIZE]  = {
	{
		"jazz",
		"50.7.98.106",
		8465
	},
	{
		"snellezender",
		"46.17.5.73",
		8662
	},
	{
		"videogamezender",
		"136.243.156.30",
		1541
	},
	{
		"radio oranje",
		"95.211.205.74",
		8000
	},
	{
		"top 40",
		"80.69.77.81",
		9150
	}
};