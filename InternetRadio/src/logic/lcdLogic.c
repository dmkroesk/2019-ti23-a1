/*
 * lcdLogic.c
 *
 * Created: 21-2-2019 10:28:18
 *  Author: Ralph Rouwen
 */ 

#define LOG_MODULE  LOG_MAIN_MODULE


#include <stdio.h>
#include <sys/thread.h>
#include <string.h>
#include <stdlib.h>

#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <avr/io.h>


#include "hardware/lcd.h"
#include "logic/timer.h"
#include "hardware/ethernet.h"

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/heap.h>

#include "logic/log.h"

#include "presentation/menu.h"
#include "presentation/navigation.h"

THREAD(ScrollThread, args) {
    
    // char* title = args;
    scrollThroughText(args);

	NutThreadExit();
}

void scroll_threadCreate(void) 
{    
    char newsTitle[50];

    ethernet_getRequest("167.114.118.40", 80, "/v2/everything?pageSize=1&q=Apple&from=2019-03-21&sortBy=popularity&apiKey=40b1cae7c83d4441841b524eb9b069b8", "newsapi.org");
    getTitle(&newsTitle);

    LogMsg_P(LOG_DEBUG, PSTR("begin thread"));  

	NutThreadCreate("ScrollThread", ScrollThread, &newsTitle,  1024);	
}



/*
 * Writes a string to the LCD screen
 *
 * param char
 *
 * return void
 *
 */
void lcd_writeString(char *string)
{
    for(; *string; string++)
    {
        LcdChar(*string);
    }
}


void LcdString(char string[]) {
    int i;
    for(i = 0; i < 15; i++)
        LcdChar(string[i+2]);
}

/*
 * 
 *
 * param char
 *
 * return void
 *
 */
void lcd_writeStringSecond(char *string)
{
    LcdSetCursor();
    lcd_writeString(string);
}

/*
 * initializes the LCD screen
 *
 * param void
 *
 * return void
 *
 */
void lcd_init(void)
{
    LcdLowLevelInit();
    //setup button listeners
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void lcd_custom_buttons()
{
	lcdCustomChar(0);
}

/*
 * initializes the LCD screen
 *
 * param void
 *
 * return void
 *
 */
void lcd_custom_internet( void ) 
{
	lcdCustomChar(1);
}

/*
 * clears the LCS screen
 *
 * param void
 *
 * return void
 *
 */
void lcd_clearDisplay(void)
{
    LcdClear();
}

/*
 * turn the backlight from the LCD screen on
 *
 * param void
 *
 * return void
 *
 */
void lcd_backlightOn(void)
{
    LcdBackLight(LCD_BACKLIGHT_ON);
}

/*
 * turn the backlight from the LCD screen off
 *
 * param void
 *
 * return void
 *
 */
void lcd_backlightOff(void)
{
    LcdBackLight(LCD_BACKLIGHT_OFF);
}

void scrollThroughText(char* text)
{
    SetCursor(0,  BOTTOM);

    // LogMsg_P(LOG_DEBUG, PSTR("Done2 %s"), text);

    // LcdString(text);

    char * t; // first copy the pointer to not change the original
    int size = 0;

    for (t = text; *t != '\0'; t++) {
        size++;
    }

    // LogMsg_P(LOG_DEBUG, PSTR("sizs %d"),size);

    char subbuff[17];
    subbuff[16] = '\0';

    int i = 0;

    for(i = 0; i < (size - 16); i++)
    {
        memcpy( subbuff, text[i], 15);
        
        // LogMsg_P(LOG_DEBUG, PSTR("Done4 %s"), &text[i]);
        SetCursor(0,  BOTTOM);

        if(menu[currentItem].id == MENU_MAIN) {
            LcdString(&text[i]);
        }

        // LcdString(&text[i]);
        NutSleep(500);
    }
}