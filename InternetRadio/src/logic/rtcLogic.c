/*
 * rtcLogic.c
 *
 * Created: 19-2-2019 16:16:36
 *  Author: Ralph Rouwen
 */ 

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include <time.h>
#include <stdio.h>
#include <string.h>

#include "hardware/system.h"
#include "hardware/uart0driver.h"
#include "hardware/rtc.h"
#include "hardware/keyboard.h"
#include "hardware/lcd.h"
	
#include "logic/lcdLogic.h"
#include "logic/log.h"
#include "logic/alarmLogic.h"
#include "logic/flash.h"
#include "logic/rtcLogic.h"

#include "presentation/menu.h"
#include "presentation/navigation.h"

#include "hardware/ethernet.h"


#define LOG_MODULE  LOG_MAIN_MODULE

//global var
char rtc_time[16];
int alarmTurnedOn = 0;
int alarmTimeOn = 1000;
char daysInMonth[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
char newsTitle[100];
int check = 0;

void rtc_update(void);

THREAD(rtc_LogicThread, arg)
{
    for(;;)
    {
		if(menu[currentItem].id == MENU_MENU || menu[currentItem].id == MENU_MAIN || menu[currentItem].id == MENU_ALARM)
		{
			rtc_update();
			if(menu[currentItem].id == MENU_MAIN) {
				if(check == 0){
					ethernet_getRequest("167.114.118.40", 80, "/v2/everything?pageSize=1&q=Apple&from=2019-03-21&sortBy=popularity&apiKey=40b1cae7c83d4441841b524eb9b069b8", "newsapi.org");
					check = 1;
				} else if (check == 1) {
					ethernet_getRequest("167.114.118.40", 80, "/v2/everything?pageSize=1&q=Apple&sortBy=popularity&apiKey=40b1cae7c83d4441841b524eb9b069b8", "newsapi.org");
					check = 0;
				}
    			getTitle(&newsTitle);
				scrollThroughText(&newsTitle);
			}
		}
        NutSleep(20000);
    }
}



/*!
 * \brief Initialize the interface to an Intersil X12xx hardware clock.
 *
 * \deprecated New applications must use NutRegisterRtc().
 *
 * \return 0 on success or -1 in case of an error.
 *
 */
int rtc_init(void)
{
    NutThreadCreate("rtc_LogicThread",rtc_LogicThread,NULL,512);
    return X12Init();
}

/*
*   \brief  Set Time and date from rtc
*/
void rtc_setTime(int sec, int min, int hour, int day, int month, int year)
{
    tm gtm;
    gtm.tm_sec = sec;
    gtm.tm_min = min;
    gtm.tm_hour = hour;
    gtm.tm_mday = day;
    gtm.tm_mon = month;
    gtm.tm_year = year;

    X12RtcSetClock(&gtm);
}

/*!
 * \brief Get date and time from the rtc.
 *
 * \param tm Points to a structure that receives the date and time
 *           information.
 *
 * \return 0 on success or -1 in case of an error.
 */
int rtc_getTime(struct _tm *time)
{
    return X12RtcGetClock(time);
}

/*
 * creates a timestamp from the rtc clock
 *
 * param int, int, int, int, int, int, char
 *
 * return void
 *
 */
void rtc_parseTimeStampToString(int hours, int min, int days, int month, int year, char *timestamp)
{
    sprintf(timestamp, "%02d:%02d %02d-%02d-%02d", hours, min, days, month, year); 
}

/*
 * creates a timestamp for the rtc clock
 *
 * param int, int char
 *
 * return void
 *
 */
void rtc_parseTimeStampHrMinToString(int hours, int min, char *timestamp)
{
	sprintf(timestamp, "%02d:%02d", hours, min);
}

/*
 * gets the time from the rtc clock
 *
 * param char
 *
 * return void
 *
 */
void rtc_getTimeAsString(char *time)
{
	struct _tm gmt;
    rtc_getTime(&gmt);
    sprintf(time,"%02d:%02d %02d-%02d-%02d", gmt.tm_hour, gmt.tm_min, gmt.tm_mday,gmt.tm_mon, (gmt.tm_year + 1900 - 2000));
}

int getMinutes()
{
	struct _tm gmt;
	rtc_getTime(&gmt);
	return gmt.tm_min;
}

/*
 * updates the rtc clock
 *
 * param void
 *
 * return void
 *
 */
void rtc_update()
{
    rtc_getTimeAsString(&rtc_time);
    LcdSetCursorBegin();
	lcd_writeString(rtc_time);
	lcd_custom_internet();
	
	if(alarmTurnedOn == false && getMinutes() != alarmTimeOn)
	{
		alarm_update();
		alarmTurnedOn = 1;
		alarmTimeOn = getMinutes();
	}
	if(getMinutes() != alarmTimeOn && alarmTimeOn != 1000)
	{
		alarmTimeOn = 1000;
		alarmTurnedOn = 0;
	}
}

/*
 * checks if the entered date is a valid date
 *
 * param int, int, int
 *
 * return bool
 *
 */
bool rtc_checkValidDate(int day, int month, int year)
{
    if (month < 1 || month > 12 || day < 0)
		return false;

	if (year % 4 == 0 && daysInMonth[month - 1] == 28)
	{
		if (day > 29)
			return false;
	}
	else if (day > daysInMonth[month - 1])
		return false;

	return true;
}
