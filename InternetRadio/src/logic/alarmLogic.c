/*
 * alarmLogic.c
 *
 * Created: 26-2-2019 12:12:10
 *  Author: Olaf
 */ 
#define LOG_MODULE  LOG_MAIN_MODULE

#include "hardware/ethernet.h"
#include "logic/alarmLogic.h"
#include "logic/flash.h"
#include "logic/log.h"
#include "logic/settings.h"
#include "logic/lcdLogic.h"
#include "logic/rtcLogic.h"
#include "logic/keyboardLogic.h"
#include "logic/radio.h"

#include "hardware/keyboard.h"

#include "logic/audio.h"
PeriodicAlarm_t *alarm1;
PeriodicAlarm_t *alarm2;
PeriodicAlarm_t *alarm3;
PeriodicAlarm_t *alarm4;
PeriodicAlarm_t *alarm5;

void alarm_resetPointers(void);

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <hardware/ethernet.h>

#include <time.h>
#include <stdio.h>
#include <string.h>

#define SNOOZE_TIMER 1
#define LOG_MODULE LOG_MAIN_MODULE

void alarm_update(void);
void checkAlarm(PeriodicAlarm_t *alarm, tm *gmt);
tm alarm_getSnoozedTime(PeriodicAlarm_t alarm);
int alarm_getSavedAlarms(void);
void alarm_playSound(void);
void alarm_stopSound(void);
int timesSnoozed = 0;

/*
THREAD(AlarmThread, arg)
{
	while (1)
	{
		LogMsg_P(LOG_INFO, PSTR("Alarm updating..."));
		alarm_update();
		NutSleep(60000);
	}
}
*/
THREAD(AlarmRunningThread, args)
{
	int *alarmPage = (int *) args;
	Settings_t *settings = flash_readSettings(SETTINGS_DATA_PAGE);
	int done = 0;
	alarm_playSound();
	while(1)
	{
			if(isLeft(KbKeyfound()) == 1)
			{
				PeriodicAlarm_t *newAlarm = flash_readPeriodicAlarm(*alarmPage);
				timesSnoozed++;
				flash_writePeriodicAlarm(*alarmPage, &newAlarm);
				free(&newAlarm);
				alarm_stopSound();
				NutThreadExit();
			}
			else if(isRight(KbKeyfound()) == 1)
			{
				timesSnoozed = 0;
				alarm_stopSound();
				NutThreadExit();
			}
			else if((done * 2 / 60) >= settings->stopalarmminutes)
			{
				timesSnoozed = 0;
				alarm_stopSound();
				NutThreadExit();
			}
			else
			{
				lcd_backlightOn();
				NutSleep(50);
				//lcd_backlightOff();
				//NutSleep(1000);
				done++;
			}
	}
}

void alarm_init()
{
	//Alarm_t existAlarm = *flash_readAlarm(ALARM_1_DATA_PAGE);
	//Alarm_t initialAlarm = {0, 0, 0, 0, 0};
	//NutThreadCreate("AlarmThread",AlarmThread,NULL,512);
	alarm_resetPointers();
}

void alarm_update(void)
{
	tm gmt;
	int i;
	rtc_getTime(&gmt);
		//TO DO remove magic numbers
	for(i = 2; i < 7; i++)
	{
		PeriodicAlarm_t alarm = alarm_getPeriodicAlarmFromMem(i);
		checkAlarm(&alarm, &gmt);
			
	}
}

void checkAlarm(PeriodicAlarm_t *alarm, tm *gmt)
{
	tm time = alarm_getSnoozedTime(*alarm);
	if(time.tm_hour == gmt->tm_hour && time.tm_min == gmt->tm_min)
	{
		NutThreadCreate("AlarmThread",AlarmRunningThread,NULL,512);
	}
}


/**
 * Switches the state of the alarm. If the alarm is on it will turn off and the other way around.
 **/
void alarm_toggle(int page)
{
	PeriodicAlarm_t newAlarm = *flash_readPeriodicAlarm(page);
	
	if(newAlarm.turnedOn)
		newAlarm.turnedOn = 0;	
	else
		newAlarm.turnedOn = 1;
	
	flash_write(page, &newAlarm, sizeof(PeriodicAlarm_t));
}

/**
 * Set the alarm time.
 **/
void alarm_setTime(int page, int hours, int minutes)
{
	PeriodicAlarm_t newAlarm = *flash_readPeriodicAlarm(page);
	
	newAlarm.hours = hours;
	newAlarm.minutes = minutes;	


	
	flash_write(page, &newAlarm, sizeof(PeriodicAlarm_t));
}

/**
 * Set the radio station the alarm plays when it goes off.
 **/
void alarm_setAudioType(int page, int audioType)
{
	PeriodicAlarm_t newAlarm = *flash_readPeriodicAlarm(page);

	//TODO zorg er voor dat het alarm een radiozender toegewezen krijgt.
	
	flash_write(page, &newAlarm, sizeof(PeriodicAlarm_t));
}

/**
 * Set the days the alarm is activated on. 
 * The array represents the weekdays where days[0] = monday etc.
 * Should only be called on planned alarms!
 **/
void alarm_setDays(int page, int days[7])
{
	//PlannedAlarm_t newAlarm = flash_readAlarm(page); //TODO make new read method for planned alarms
	
	//int i = 0;
	//for(i = 0; i < 6; i++)
	//{
		//newAlarm.days[i] = days[i];
	//}	
	
	//flash_write(page, &newAlarm, sizeof(PeriodicAlarm_t)); //TODO make new write method for planned alarms
}

/*
 * Adds an alarm to the flash memory
 *
 * param hours, minutes, audiotype, turnedOn
 *
 * return void
 *
 */
void alarm_addNewAlarm(int hours, int minutes, int audioType, int turnedOn)
{
	PeriodicAlarm_t newAlarm = {hours, minutes, audioType, turnedOn};
	int page = alarm_getSavedAlarms();
	flash_writePeriodicAlarm(page, &newAlarm);
}


/*
 * Gets a periodic alarm from the memory
 *
 * param page
 *
 * return void
 *
 */
PeriodicAlarm_t alarm_getPeriodicAlarmFromMem(int page)
{
	return *flash_readPeriodicAlarm(page);
}

/*
 * gets all the saved alarms from the flash memory
 *
 * param void
 *
 * return the alarmor 0
 *
 */
void alarm_savePeriodicAlarmGlobally()
{
	AlarmTable_t *alarm = flash_readAlarmTable();
	alarm_resetPointers();

	if(alarm->alarm1Active == true)
	{
		alarm1 = flash_readPeriodicAlarm(ALARM1_DATA_PAGE);
	}
	if(alarm->alarm2Active == true)
	{
		alarm2 = flash_readPeriodicAlarm(ALARM2_DATA_PAGE);
	}
	if(alarm->alarm3Active == true)
	{
		alarm3 = flash_readPeriodicAlarm(ALARM3_DATA_PAGE);
	}
	if(alarm->alarm4Active == true)
	{
		alarm4 = flash_readPeriodicAlarm(ALARM4_DATA_PAGE);
	}
	if(alarm->alarm5Active == true)
	{
		printf("6");
		alarm5 = flash_readPeriodicAlarm(ALARM5_DATA_PAGE);
	}	
	free(alarm);
}

void alarm_getAlarmList(PeriodicAlarm_t *alarms[5])
{
	alarms[0] = alarm1;
	alarms[1] = alarm2;
	alarms[2] = alarm3;
	alarms[3] = alarm4;
	alarms[4] = alarm5;
}

int alarm_getSavedAlarms(void)
{
	int page = 6;
	AlarmTable_t *alarm = flash_readAlarmTable();
	
	if(alarm->alarm1Active == false)
	{
		alarm->alarm1Active = true;
		page = ALARM1_DATA_PAGE;
	}
	else if(alarm->alarm2Active == false)
	{
		alarm->alarm2Active = true;
		page = ALARM2_DATA_PAGE;
	}
	else if(alarm->alarm3Active == false)
	{
		alarm->alarm3Active = true;
		page = ALARM3_DATA_PAGE;
	}
	else if(alarm->alarm4Active == false)
	{
		alarm->alarm4Active = true;
		page = ALARM4_DATA_PAGE;
	}
	else if(alarm->alarm5Active == false)
	{
		alarm->alarm5Active = true;
		page = ALARM5_DATA_PAGE;
	}
	
	flash_writeAlarmTable(alarm);
	free(alarm);
	return page;
}

void alarm_resetPointers(void)
{
	alarm1 = NULL;
	alarm2 = NULL;
	alarm3 = NULL;
	alarm4 = NULL;
	alarm5 = NULL;
}


tm alarm_getSnoozedTime(PeriodicAlarm_t alarm)
{
	tm time;
	if(alarm.minutes + alarm.timesSnoozed * SNOOZE_TIMER < 60)
	{
		time.tm_hour = alarm.hours;
		time.tm_min = alarm.minutes + SNOOZE_TIMER * timesSnoozed;
	}
	else
	{
		time.tm_hour = alarm.hours + 1;
		time.tm_min = (alarm.minutes + timesSnoozed * SNOOZE_TIMER) % 60;
	}
	return time;
} 

void alarm_playSound(void)
{
	//TODO: check if there is internet
	printf("Connection: %i", ethernet_checkconnection());
	
	if(ethernet_checkconnection())
	{
		printf("ik speel audio van radiointernet 1");
		ethernet_connectStream(&radioStationList[1]);	
	}
	else
	{	
		printf("ik speel audio van radiointernet 2");
		audio_playTest();	
	}
}

void alarm_stopSound(void)
{
	audio_stopPlayback();
}
