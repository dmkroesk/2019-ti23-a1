/*
 * audio.c
 *
 * Created: 12/03/2019 10:30:32
 *  Author: Tim Herreijgers
 */ 

#include <sys/heap.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/types.h>

#include <math.h>
#include <stdbool.h>

#include "stdio.h"

#include "hardware/vs10xx.h"
#include "logic/marioSongData.h"
#include "logic/audio.h"
#include "logic/flash.h"
#include "logic/log.h"
#include "hardware/spidrv.h"

void thread_playTest(void);
void thread_playStream(void *);

bool running;
int playingStream;

THREAD(audio_thread, args)
{
	running = true;
    if(args == 0x00)
    {
        thread_playTest();
    }
    else
    {
        thread_playStream(args);
    }
	NutThreadExit();
}

void thread_playTest()
{
    int i;
    u_char freq;
    for(i = 0; i < SONG_LENGTH && running; i++)
    {
        if(notes[i] == 0)
        {
            NutSleep(duration[i] * 2);
            continue;
        }
        freq = floor(notes[i] / 86);
        VsBeep(freq, duration[i] * 2);
    }
	NutThreadExit();
}

void thread_playStream(void *stream)
{
	playingStream = 1;
  int rbytes = 0;
	char *mp3buf;
	int nrBytesRead = 0;
	unsigned char iflag;
	
	//
	// Init MP3 buffer. NutSegBuf is een globale, systeem buffer
	//
	if( 0 != NutSegBufInit(8192) )
	{
		// Reset global buffer
		iflag = VsPlayerInterrupts(0);
		NutSegBufReset();
		VsPlayerInterrupts(iflag);
		
	}
	
	VsPlayerReset(0);
	VsPlayerSetMode(VS_SM_STREAM);

	for(;playingStream != 0;)
	{
		/*
		 * Query number of byte available in MP3 buffer.
		 */
    iflag = VsPlayerInterrupts(0);
    mp3buf = NutSegBufWriteRequest(&rbytes);
    VsPlayerInterrupts(iflag);
		
		// Bij de eerste keer: als player niet draait maak player wakker (kickit)
		if( VS_STATUS_RUNNING != VsGetStatus() )
		{
			if( rbytes < 1024 )
			{
				VsPlayerKick();
				printf("\nKicking player");
			}
		}
		
		while( rbytes )
		{
			// Copy rbytes (van 1 byte) van stream naar mp3buf.
			nrBytesRead = fread(mp3buf,1,rbytes,stream);
			
			if( nrBytesRead > 0 )
			{
				iflag = VsPlayerInterrupts(0);
				mp3buf = NutSegBufWriteCommit(nrBytesRead);
				VsPlayerInterrupts(iflag);
				if( nrBytesRead < rbytes && nrBytesRead < 512 )
				{
					NutSleep(250);
				}
			}
			else
			{
				break;
			}
			rbytes -= nrBytesRead;
			
			if( nrBytesRead <= 0 )
			{
				break;
			}				
		}
	}
}

void audio_playMp3Stream(void *stream)
{
	printf("\nStarting in audio_playMp3Stream");
	playingStream = 1;
  int rbytes = 0;
	char *mp3buf;
	int nrBytesRead = 0;
	int first = 1;
	unsigned char iflag;
	
	SPIdeselect();
	SPIselect(SPI_DEV_VS10XX);

	//
	// Init MP3 buffer. NutSegBuf is een globale, systeem buffer
	//
	if( 0 != NutSegBufInit(8192) )
	{
		// Reset global buffer
		iflag = VsPlayerInterrupts(0);
		NutSegBufReset();
		VsPlayerInterrupts(iflag);
		
	}
	
	VsPlayerReset(0);
	VsPlayerSetMode(VS_SM_STREAM);

	for(;playingStream != 0;)
	{
		printf("\ntest");
		/*
		 * Query number of byte available in MP3 buffer.
		 */
    iflag = VsPlayerInterrupts(0);
    mp3buf = NutSegBufWriteRequest(&rbytes);
    VsPlayerInterrupts(iflag);
		
		// Bij de eerste keer: als player niet draait maak player wakker (kickit)
		if( VS_STATUS_RUNNING != VsGetStatus() )
		{
			printf("\nPlayer is not running! gonna kick it now: %d", VsGetStatus());
			if( first )
			{
				VsPlayerKick();
				printf("\nKicking player: %d", VsGetStatus());
				first = 0;
			}
		}

		while( rbytes )
		{
			// Copy rbytes (van 1 byte) van stream naar mp3buf.
			nrBytesRead = fread(mp3buf,1,rbytes,stream);
			printf("\nnrBytesRead: %d", nrBytesRead);
			
			if( nrBytesRead > 0 )
			{
				iflag = VsPlayerInterrupts(0);
				mp3buf = NutSegBufWriteCommit(nrBytesRead);
				VsPlayerInterrupts(iflag);
				if( nrBytesRead < rbytes && nrBytesRead < 512 )
				{
					NutSleep(250);
				}
			}
			else
			{
				break;
			}
			rbytes -= nrBytesRead;
			
			if( nrBytesRead <= 0 )
			{
				break;
			}				
		}
	}
}

void audio_stopStream()
{
	playingStream = 0;
	printf("Closed stream audio");
}

void audio_init(void)
{
    VsPlayerInit();
	audio_settingsInit();
}

/**
  * Restores the saved audio settings from flash memory.
  **/
void audio_settingsInit(void)
{
	AudioSettigns_t* settings = flash_readAudioSettings();
	VsSetVolume(settings->leftVol, settings->rightVol);
	audio_setBass(settings->bass);	
	audio_setTreble(settings->treble);
	printf("Opstart test %d", audio_getBass());
	free(settings);
}

void audio_playTest(void)
{
    NutThreadCreate("PlayerThread", audio_thread, 0x00, 512);
}

void audio_playStream(void *stream)
{
    NutThreadCreate("PlayerThread", audio_thread, stream, 512);
}

void audio_stopPlayback(void)
{
	running = false;
}

/**
  * Returns the volume of the left speaker. Needed for potential balance settings.
  **/
int audio_getLeftVol()
{
	u_short volume = VsGetVolume();
	return (volume >> 8) & 0xFF;
}

/**
  * Returns the volume of the right speaker. Needed for potential balance settings.
  **/
int audio_getRightVol()
{
	u_short volume = VsGetVolume();
	return volume & 0xFF;
}

/**
  * CAUTION: Volume min is 0xFE (15) and volume max = 0x00 (0).
  *			 See vs10xx documentation H8.6.11 for more info.
  *	This method contains an if-statement for potential balance setting
  * After the button is pressed, the volume is immediately saved to flash memory.
  **/
void audio_volUp(void)
{
	AudioSettigns_t* settings = flash_readAudioSettings();
	if((audio_getLeftVol() > 0) && (audio_getRightVol() > 0)) //Check if volume level isn't at highest value possible already
	{
		VsSetVolume(audio_getLeftVol() - 1, audio_getRightVol() - 1);
		settings->leftVol = settings->leftVol - 1;
		settings->rightVol = settings->rightVol - 1;
	}
	else if((audio_getLeftVol() > 0) && (audio_getRightVol() == 0))
	{
		VsSetVolume(audio_getLeftVol() - 1, 0);
		settings->leftVol--;
	}
	else if((audio_getLeftVol() == 0) && (audio_getRightVol() > 0))
	{
		VsSetVolume(0, audio_getRightVol() - 1);
		settings->rightVol--;
	}
	flash_writeAudioSettings(settings);
	free(settings);
}

/**
  * CAUTION: Volume min is 0xFE (15) and volume max = 0x00 (0).
  *			 See vs10xx documentation H8.6.11 for more info.	
  *	This method contains an if-statement for potential balance setting	
  * After the button is pressed, the volume is immediately saved to flash memory.		
  **/
void audio_volDown(void)
{
	AudioSettigns_t* settings = flash_readAudioSettings();
	if((audio_getLeftVol() < 15) && (audio_getRightVol() < 15)) //Check if volume level isn't at lowest value possible already
	{
		VsSetVolume(audio_getLeftVol() + 1, audio_getRightVol() + 1);
		settings->leftVol++;
		settings->rightVol++;
	}
	else if((audio_getLeftVol() < 15) && (audio_getRightVol() == 15))
	{
		VsSetVolume(audio_getLeftVol() + 1, 15);
		settings->leftVol++;
	}
	else if((audio_getLeftVol() == 15) && (audio_getRightVol() < 15))
	{
		VsSetVolume(15, audio_getRightVol() + 1);
		settings->rightVol++;
	}
	flash_writeAudioSettings(&settings);
	free(settings);
}

/**
  * Returns the current bass value.
  * Bass 0 = off, 15 is highest
  **/
int  audio_getBass(void)
{
	return (VsRegInfo(VS_BASS_REG) >> 3) & 0xEF;
}

/**
  * Writes the given bass value to the system.
  **/
void audio_setBass(u_int bass)
{
	u_int bassReg = VsRegInfo(VS_BASS_REG) & 0xFF87;
	printf("Bassreg: %x \n",bassReg);
	bassReg |= (bass << 3);
	VsRegWrite(VS_BASS_REG, bassReg);
}

/**
  * Takes the current bass value and increases it by 1 if it wasn't at it's highest possible value yet.
  **/
void audio_bassUp(void)
{
	int bass = audio_getBass();
	
	if(bass < 15)
	{
		bass++;
		audio_setBass(bass);
	}
}

/**
  * Takes the current bass value and decreases it by 1 if it wasn't at it's lowest possible value yet.
  **/
void audio_bassDown(void)
{
	int bass = audio_getBass();
	
	if(bass > 0)
	{
		bass--;
		audio_setBass(bass);
	}
}

/**
  * Returns the current bass value.
  * Treble 0 = off, 15 is highest
  **/
int audio_getTreble(void)
{
	return (VsRegInfo(VS_BASS_REG) >> 12);
}

/**
  * Writes the given treble value to the system.
  **/
void audio_setTreble(u_int treble)
{
	u_int trebReg = VsRegInfo(VS_BASS_REG) & 0x0FFF;
	trebReg |= (treble << 12);
	VsRegWrite(VS_BASS_REG, trebReg);
}

/**
  * Takes the current treble value and increases it by 1if it wasn't at it's highest possible value yet.
  **/
void audio_trebleUp(void)
{
	int treble = audio_getTreble();
	printf("treble1 + = %d \n", treble);
	
	if(treble < 15)
	{
		treble++;
		audio_setTreble(treble);
	}
}

/**
  * Takes the current treble value and decreases it by 1 if it wasn't at it's lowest possible value yet.
  **/
void audio_trebleDown(void)
{
	int treble = audio_getTreble();
	
	if(treble > 0)
	{
		treble = treble - 1;
		audio_setTreble(treble);
	}
}