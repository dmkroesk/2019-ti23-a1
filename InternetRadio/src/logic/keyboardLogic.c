/*
 * KeyboardLogic.c
 *
 * Created: 21-2-2019 09:49:13
 *  Author: Olaf
 */ 
#define LOG_MODULE  LOG_MAIN_MODULE

#define SETTING_TIME 0
#define INCORRECT_TIME 1

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "string.h"

#include "hardware/keyboard.h"
#include "hardware/lcd.h"

#include "presentation/navigation.h"
#include "presentation/menu.h"

#include "logic/log.h"
#include "logic/flash.h"
#include "logic/settings.h"
#include "logic/rtcLogic.h"
#include "logic/alarmLogic.h"
#include "logic/flash.h"
#include "logic/lcdLogic.h"
#include "logic/keyboardLogic.h"
#include "logic/radio.h";
#include "hardware/ethernet.h";

static keyCallback_t *callbacks;
static int callbackLength = 0;
static int rising_edge = 0;

static int hours, min, sec, days, month, year, off, sound = 0;
static int placement = 0;
static int placement_alarm = 0;
static int placement_alarm_ok = 0;
static char timestamp[16];
static int currentShownAlarm = 0;
static int infraredOn = 0;
char setTimeState = SETTING_TIME;
static int infraredChanged = 1;

int alarm_stopminutes = 0;
char minutes[7];

char macArray  [16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
int  macPlaces [12] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
char macAddress[13] = {'0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '\0'};	
	
int placement_MAC = 0;

Settings_t *settings;

/*
 * checks if a key is pressed continual
 *
 * param none
 *
 * return void
 *
 */
void kbl_update()
{
	int currentKey = KbKeyfound();
	
	if(rising_edge == 0 && currentKey != 136)
	{
		rising_edge = 1;
		if(currentKey != KEY_UNDEFINED && currentKey != KEY_NO_KEY)
		{
			int i;
			for(i = 0; i < callbackLength; i++)
			{
				callbacks[i](currentKey);
			}
		}
	}
	else if(currentKey == 136)
	{
		rising_edge = 0;
	}
}

/*
 * initializes the keyboard
 *
 * param none
 *
 * return void
 *
 */
void kbl_init()
{
	kbl_addCallback(navigation_HandleKeypress);
	LogMsg_P(LOG_DEBUG, PSTR("Test"));
	navigation_initMenu();
	LogMsg_P(LOG_DEBUG, PSTR("Test2"));

}

/*
 * 
 *
 * param keyCallback_t
 *
 * return void
 *
 */
void kbl_addCallback(keyCallback_t callback)
{
	keyCallback_t *tempCallbacks = malloc(sizeof(callbacks) * (callbackLength + 1));
	memcpy(tempCallbacks, callbacks, sizeof(keyCallback_t) * callbackLength);

	if(callbackLength > 0)
	free(callbacks);

	tempCallbacks[callbackLength] = callback;
	callbacks = tempCallbacks;
	callbackLength++;
}

/*
 * checks if the OK button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_Ok(void)
{	
	if((menu[currentItem].OnExit) != NULL)
	{
		(*menu[currentItem].OnExit)();	
	}
	
	if(menu[currentItem].selectedItemId[currentSelectedItem] != NULL)
	{
		currentId = menu[currentItem].selectedItemId[currentSelectedItem];
	}	
	
	currentItem = 0;

	while (menu[currentItem].id != currentId)
	{
		currentItem += 1;
	}
	
	if (menu[currentItem].OnEntry != NULL)
	{
		(*menu[currentItem].OnEntry)();
	}
}

/*
 * checks if the ESC button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_Esc(void)
{
	resetVariables();
	LogMsg_P(LOG_INFO, PSTR("Esc pressed"));
	LogMsg_P(LOG_DEBUG, PSTR("Esc start %d"), menu[currentItem].nextId[KEY_ESC]);
	
	if(menu[currentItem].nextId[KEY_ESC] != CURRENT_MENU)
	{
		if((menu[currentItem].OnExit) != NULL)
		{
			(*menu[currentItem].OnExit)();
			LogMsg_P(LOG_DEBUG, PSTR("On Exit %i"), menu[currentItem].id);
		}
		
		currentId = menu[currentItem].nextId[KEY_ESC];
		currentItem = 0;
		currentSelectedItem = 0;
		
		while (menu[currentItem].id != currentId)
		{
			currentItem += 1;
		}
		
		if (menu[currentItem].OnEntry != NULL)
		{
			(*menu[currentItem].OnEntry)();
			LogMsg_P(LOG_DEBUG, PSTR("On Entry %i"), menu[currentItem].id);
		}
	}
}

/*
 * checks if the up arrow button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_Up(void)
{
	LogMsg_P(LOG_INFO, PSTR("Up pressed"));
	if(currentSelectedItem > 0)
	{
		currentSelectedItem = currentSelectedItem - 1;
		updateSelectedItem();
	}
}

/*
 * checks if the down arrow button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_Down()
{
	LogMsg_P(LOG_INFO, PSTR("Down pressed"));
	if(menu[currentItem].selectedItem[currentSelectedItem] != 0x00)
	{
		if(menu[currentItem].selectedItem[currentSelectedItem + 1] != 0x00)
		{
			currentSelectedItem = currentSelectedItem + 1;
			updateSelectedItem();
		}
	}
}

void pressed_power(void)
{
	ethernet_closeStream();
	LogMsg_P(LOG_INFO, PSTR("Closed stream"));
}

void infrared_Pressed(int a)
{
	navigation_HandleKeypress(a);
}

/*
 * checks if the left arrow button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_Left(void)
{
	
	LogMsg_P(LOG_INFO, PSTR("Volume down"));
}

/*
 * checks if the right arrow button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_Right(void)
{
	LogMsg_P(LOG_INFO, PSTR("Volume up"));
}

/*
 * checks if the 1 button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_1(void)
{
	LogMsg_P(LOG_INFO, PSTR("Pressed button 1"));
	//pressed_power();

	ethernet_connectStream(&radioStationList[1]);
}

/*
 * checks if the 2 button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_2(void)
{
	LogMsg_P(LOG_INFO, PSTR("Play favoriet van zender 2 af"));
	
	ethernet_connectStream(&radioStationList[0]);
}

/*
 * checks if the 3 button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_3(void)
{
	LogMsg_P(LOG_INFO, PSTR("Play favoriet van zender 3 af"));
	
	ethernet_connectStream(&radioStationList[2]);
}

/*
 * checks if the 4 button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_4(void)
{
	LogMsg_P(LOG_INFO, PSTR("Play favoriet van zender 4 af"));
	ethernet_connectStream(&radioStationList[3]);
}

/*
 * checks if the 5 button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_5(void)
{
	LogMsg_P(LOG_INFO, PSTR("Play favoriet van zender 5 af"));
	ethernet_connectStream(&radioStationList[4]);
}

/*
 * checks if the next button is pressed
 *
 * param void
 *
 * return void
 *
 */
void pressed_next(void)
{
	if(placement > 0){
		placement--;
	}
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void pressed_prev(void)
{
	if(placement < 4){
		placement++ ;
	}
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void pressed_increment_alarmTime(void)
{
	if(alarm_stopminutes >= 0)
	{
		alarm_stopminutes++;
		lcd_clearDisplay();
		sprintf(minutes, "%02d minutes", alarm_stopminutes); 
		lcd_writeString("Enter stop time");
		lcd_writeStringSecond(minutes);
	}
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void pressed_decrement_alarmTime(void)
{
	if(alarm_stopminutes > 0)
	{
		alarm_stopminutes--;
		lcd_clearDisplay();
		sprintf(minutes, "%02d minutes", alarm_stopminutes); 
		lcd_writeString("Enter stop time");
		lcd_writeStringSecond(minutes);
	}
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void pressed_alarmstop_accept(void)
{
	Settings_t settings;
	settings.stopalarmminutes = alarm_stopminutes;
    flash_writeSettings(SETTINGS_DATA_PAGE, &settings);
	LogMsg_P(LOG_DEBUG, PSTR("Save alarm stop settings %d"), settings.stopalarmminutes);
	navigation_HandleKeypress(KEY_ESC);	
}

void MAC_pressed_next(void)
{
	if(placement_MAC < 12){
		placement_MAC++;
	}
}

void MAC_pressed_prev(void)
{
	if(placement_MAC > 0){
		placement_MAC--;
	}
}

void MAC_increment(void)
{
	if(macPlaces[placement_MAC] < 15)
	{
		macPlaces[placement_MAC] += 1;
	}

	macAddress[placement_MAC] = macArray[macPlaces[placement_MAC]];
	LogMsg_P(LOG_DEBUG, PSTR("MACaddresss: %s"), macAddress);
	lcd_writeStringSecond(macAddress);
}

void MAC_decrement(void)
{
	if(macPlaces[placement_MAC] > 0)
	{
		macPlaces[placement_MAC] -= 1;
	}

	macAddress[placement_MAC] = macArray[macPlaces[placement_MAC]];
	LogMsg_P(LOG_DEBUG, PSTR("MACaddress: %s"), macAddress);
	lcd_writeStringSecond(macAddress);
}

void MAC_pressed_ok()
{
	flash_writeMACAddress(macAddress);
	navigation_HandleKeypress(KEY_ESC);	
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void pressed_increment(void)
{
	if(placement == 0 && hours < 23) {
		hours++;
		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}
	else if(placement == 1 && min < 59){
		min++;
		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}
	else if(placement == 2 && days < 31)
	{
		days++;
		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}
	else if(placement == 3 && month < 12)
	{
		month++;
		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}
	else if(placement == 4 && year < 99)
	{
		year++;
		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}
}

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void pressed_decrement(void)
{
	if(placement == 0 && hours > 0) 
	{
		hours--;
		
		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}
	else if(placement == 1 && min > 0)
	{
		min--;

		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}
	else if(placement == 2 && days > 0)
	{
		days--;

		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}
	else if(placement == 3 && month > 0)
	{
		month--;

		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}
	else if(placement == 4 && year > 0)
	{
		year--;

		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
	}

} 

/*
 * 
 *
 * param void
 *
 * return void
 *
 */
void pressed_time_accept()
{
	if(setTimeState == SETTING_TIME) 
	{
		if(rtc_checkValidDate(days, month, year))
		{
			rtc_setTime(sec, min, hours, days, month, year);
            resetVariables();
			rtc_update();
			navigation_HandleKeypress(KEY_ALT);
		} 
		else
		{
			lcd_clearDisplay();
			lcd_writeString("Invalid time");
			setTimeState = INCORRECT_TIME;
		}
	}
	else if(setTimeState == INCORRECT_TIME)
	{
		rtc_parseTimeStampToString(hours, min, days, month, year, timestamp);
		lcd_clearDisplay();
		lcd_writeString(timestamp);
		setTimeState = SETTING_TIME;
	}
}

void favorites_pressed_right()
{
	int a = flash_readFavorite(currentSelectedItem);
	
	if(a < RADIO_STATION_LIST_SIZE)
	{
		flash_writeFavorite(currentSelectedItem, (a + 1));
		lcd_writeStringSecond(radioStationList[a + 1].name);		
	}
}

void favorites_pressed_left()
{
	int a = flash_readFavorite(currentSelectedItem);
	
	if(a > 0)
	{
		flash_writeFavorite(currentSelectedItem, (a - 1));
		lcd_writeStringSecond(radioStationList[a - 1].name);
	}
}

/*
 * updates the LCD screen 
 *
 * param void
 *
 * return void
 *
 */
void updateSelectedItem(void)
{
	lcd_clearDisplay();
	
	switch(menu[currentItem].id)
	{
		case MENU_MENU:
		case MENU_ALARM:
			rtc_update();
		break;
		
		case MENU_SETTINGS:
			lcd_writeString("Settings");
		break;
		
		case MENU_ALARM_ADD:
			lcd_writeString("Add alarm");	
		break;	
		
		case MENU_FAVORITES:
			lcd_writeString("Favorites");
		break;
		case MENU_AUDIO:
			lcd_writeString("Audio Settings");
		break;
	}

	lcd_writeStringSecond(menu[currentItem].selectedItem[currentSelectedItem]);
	lcd_custom_buttons();
	LogMsg_P(LOG_INFO, PSTR("Update selected item"));
}

/*
 * checks if the key that is pressed is one of the left buttons
 *
 * param int
 *
 * return int
 *
 */
int isLeft(int key)
{
	switch (key)
	{
	case KEY_01:
	case KEY_02:
	case KEY_03:
	case KEY_04:
	case KEY_05:
	case KEY_ALT:
		return 1;
	}
	return 0;
}

/*
 * checks if the key that is pressed is one of the right buttons
 *
 * param int
 *
 * return int
 *
 */
int isRight(int key)
{	
	switch (key)
	{
		case KEY_ESC:
		case KEY_UP:
		case KEY_DOWN:
		case KEY_LEFT:
		case KEY_RIGHT:
		case KEY_OK:
		return 1;
	}
	return 0;
}

/*
 *
 *
 * param void
 *
 * return void
 *
 */
void pressed_next_alarm(void)
{
	if(placement_alarm > 0 && placement_alarm_ok == 0)
		placement_alarm--;
	
	LogMsg_P(LOG_INFO, PSTR("Placement: %i"),placement);
}

/*
 *
 *
 * param void
 *
 * return void
 *
 */
void pressed_prev_alarm(void)
{
	if(placement_alarm < 2 && placement_alarm_ok == 0)
		placement_alarm++;
		
	LogMsg_P(LOG_INFO, PSTR("Placement: %i"),placement);
}

/*
 *
 *
 * param void
 *
 * return void
 *
 */
void pressed_increment_alarm(void)
{
	switch(placement_alarm_ok)
	{
		case 0:
			if(placement_alarm == 0 && hours < 23)
			{
				hours++;
				rtc_parseTimeStampHrMinToString(hours, min, timestamp);
				lcd_writeStringSecond(timestamp);
				LogMsg_P(LOG_DEBUG, PSTR("hours increase++"));
			}
			else if(placement_alarm == 1 && min < 59)
			{
				min++;
				rtc_parseTimeStampHrMinToString(hours, min, timestamp);
				lcd_writeStringSecond(timestamp);
				LogMsg_P(LOG_DEBUG, PSTR("mins increase++"));
			}
			break;
		case 1:
			if(off == 0)
			{
				lcd_clearDisplay();
				lcd_writeString("Add alarm");
				off = 1;
				lcd_writeStringSecond("Off");
			}
			else
			{
				lcd_clearDisplay();
				lcd_writeString("Add alarm");
				off = 0;
				lcd_writeStringSecond("On");
			}
			break;
		case 2:
			break;
	}
}

/*
 *
 *
 * param void
 *
 * return void
 *
 */
void pressed_decrement_alarm(void)
{
	switch(placement_alarm_ok)
	{
		case 0:
			if(placement_alarm == 0 && hours > 0)
			{
				hours--;

				rtc_parseTimeStampHrMinToString(hours, min, timestamp);
				lcd_writeStringSecond(timestamp);
				LogMsg_P(LOG_DEBUG, PSTR("decrement++"));
			}
			else if(placement_alarm == 1 && min > 0)
			{
				min--;

				rtc_parseTimeStampHrMinToString(hours, min, timestamp);
				lcd_writeStringSecond(timestamp);
				LogMsg_P(LOG_DEBUG, PSTR("decrement++"));
			}
			break;
		case 1:
			if(off == 0)
			{
				lcd_clearDisplay();
				lcd_writeString("Add alarm");
				off = 1;
				lcd_writeStringSecond("Off");
			}
			else
			{
				lcd_clearDisplay();
				lcd_writeString("Add alarm");
				off = 0;
				lcd_writeStringSecond("On");
			}
			break;
		case 2:
			break;
	}	
}

/*
 *
 *
 * param void
 *
 * return void
 *
 */
void pressed_accept_alarm()
{
	switch(placement_alarm_ok)
	{
		case 0:
		case 1:
			placement_alarm_ok++;
			pressed_Down();
			break;
		case 2:
			alarm_addNewAlarm(hours, min, sound, off);
			resetVariables();
			navigation_HandleKeypress(KEY_ESC);		
			LogMsg_P(LOG_INFO, PSTR("----------Added Alarm------------"));
			getAlarms();
			break;
	}
}

void pressed_bass_Down(void)
{
	lcd_clearDisplay();
	char* timeString[15];
	rtc_getTimeAsString(&timeString);
	lcd_writeString(timeString);
	audio_bassDown();
	char* bassString[3];
	sprintf(bassString, "Bass: %d \n", audio_getBass());
	lcd_writeStringSecond(bassString);
}

void pressed_bass_Up(void)
{
	lcd_clearDisplay();
	char* timeString[15];
	rtc_getTimeAsString(&timeString);
	lcd_writeString(timeString);
	audio_bassUp();
	char* bassString[3];
	sprintf(bassString, "Bass: %d \n", audio_getBass());
	lcd_writeStringSecond(bassString);
}

void pressed_treble_Up(void)
{
	lcd_clearDisplay();
	char* timeString[15];
	rtc_getTimeAsString(&timeString);
	lcd_writeString(timeString);
	audio_trebleUp();
	char* trebleString[11];
	sprintf(trebleString, "Treble: %d", audio_getTreble() - 8);
	lcd_writeStringSecond(trebleString);
}

void pressed_treble_Down(void)
{
	lcd_clearDisplay();
	char* timeString[15];
	rtc_getTimeAsString(&timeString);
	lcd_writeString(timeString);
	audio_trebleDown();
	char* trebleString[11];
	sprintf(trebleString, "Treble: %d", audio_getTreble() - 8);
	lcd_writeStringSecond(trebleString);
}

void pressed_bass_Ok(void)
{
	AudioSettigns_t* settings = flash_readAudioSettings();
	settings->bass = audio_getBass();
	flash_writeAudioSettings(settings);
	free(settings);
	pressed_Esc();
}

void pressed_treble_Ok(void)
{
	AudioSettigns_t* settings = flash_readAudioSettings();
	settings->treble = audio_getTreble();
	flash_writeAudioSettings(settings);
	free(settings);
	pressed_Esc();
}

/**
  *	Different method than updateSelectedItem, because the time is written on the upper line in this method.
  **/
void pressed_audio_Up(void)
{
	
	
	if(menu[currentItem].selectedItem[currentSelectedItem] != 0x00)
	{
		if(menu[currentItem].selectedItem[currentSelectedItem - 1] != 0x00)
		{
			switch(menu[currentItem].id)
			{
				case MENU_MENU:
				case MENU_ALARM:
				rtc_update();
				break;
				
				case MENU_SETTINGS:
				lcd_writeString("Settings");
				break;
				
				case MENU_ALARM_ADD:
				lcd_writeString("Add alarm");
				break;
				
				case MENU_FAVORITES:
				lcd_writeString("Favorites");
				break;
				
				case MENU_AUDIO:
				lcd_writeString("Audio");
				break;
			}

			lcd_writeStringSecond(menu[currentItem].selectedItem[currentSelectedItem]);
			lcd_custom_buttons();
		}
	}
}

/**
  *	Different method than updateSelectedItem, because the time is written on the upper line in this method.
  **/
void pressed_audio_Down(void) 
{
	lcd_clearDisplay();
	
	char* timeString;
	rtc_getTimeAsString(timeString);
	lcd_writeString(*timeString);
	
	switch(menu[currentItem].id)
	{
		case MENU_MENU:
		case MENU_ALARM:
		rtc_update();
		break;
		
		case MENU_SETTINGS:
		lcd_writeString("Settings");
		break;
		
		case MENU_ALARM_ADD:
		lcd_writeString("Add alarm");
		break;
		
		case MENU_FAVORITES:
		lcd_writeString("Favorites");
		break;
	}

	lcd_writeStringSecond(menu[currentItem].selectedItem[currentSelectedItem]);
	lcd_custom_buttons();	
}

/*
 * resets all the variables that are declared by the user
 *
 * param void
 *
 * return void
 *
 */
void resetVariables()
{
	hours = 0; min = 0; sec = 0; days= 0; 
	month = 0; year = 0; off = 0, sound = 0;
	placement = 0;
	placement_alarm = 0;
	placement_alarm_ok = 0;
	infraredOn = 0;
	int i = 0;
	for(i = 0; i < 12; i++)
	{
		macPlaces[i] = 0;
		macAddress[i] = '0';
	}
	
	placement_MAC = 0;
}

/*
 * gets all the current alarms
 *
 * param void
 *
 * return void
 *
 */
void getAlarms(void)
{
	//Maak array aan (leeg)
	//Roep alarm_getAsList aan (met die array)
	//Array is gevuld met alarmen
	LogMsg_P(LOG_DEBUG, PSTR("Alarm 1 %d"), alarm1->hours);
	LogMsg_P(LOG_DEBUG, PSTR("Alarm 2 %d"), alarm2->hours);
	LogMsg_P(LOG_DEBUG, PSTR("Alarm 3 %d"), alarm3->hours);
	LogMsg_P(LOG_DEBUG, PSTR("Alarm 4 %d"), alarm4->hours);
	LogMsg_P(LOG_DEBUG, PSTR("Alarm 5 %d"), alarm5->hours);
}

/*
 *
 *
 * param void
 *
 * return void
 *
 */
void kbl_keyPressedDownAlarmDelete(void)
{
	kbl_showAlarms(1);
}

/*
 *
 *
 * param void
 *
 * return void
 *
 */
void kbl_keyPressedUpAlarmDelete(void)
{
	kbl_showAlarms(-1);
}

/*
 *
 *
 * param void
 *
 * return void
 *
 */
void kbl_showAlarms(int change)
{
	PeriodicAlarm_t *alarms[5] = {alarm1, alarm2, alarm3, alarm4, alarm5};
	char alarmTimeStamp[16];

	if(currentShownAlarm + change > -1 && currentShownAlarm + change < 5)
	{
		if(alarms[currentShownAlarm + change] == NULL)
		{
			kbl_showAlarms(change < 0 ? change - 1 : change + 1);
		}
		else
		{
			currentShownAlarm += change;
			LogMsg_P(LOG_DEBUG, PSTR("Currently showing the %d alarm"), currentShownAlarm);
			rtc_parseTimeStampHrMinToString(alarms[currentShownAlarm]->hours, 
											alarms[currentShownAlarm]->minutes, alarmTimeStamp);
			lcd_writeStringSecond(alarmTimeStamp);
		}
	}
}

/*
 *
 *
 * param void
 *
 * return void
 *
 */
void kbl_deleteAlarm()
{
	AlarmTable_t *table = flash_readAlarmTable();
	switch(currentShownAlarm)
	{
		case 0:
			table->alarm1Active = false;
			break;
		case 1:
			table->alarm2Active = false;
			break;
		case 2:
			table->alarm3Active = false;
			break;
		case 3:
			table->alarm4Active = false;
			break;
		case 4:
			table->alarm5Active = false;
			break;
	}
	flash_writeAlarmTable(table);
	free(table);
	alarm_savePeriodicAlarmGlobally();
	navigation_HandleKeypress(KEY_ESC);
}

void infrared_Pressed_Left_Or_Right(void)
{
	if(infraredOn == 0)
	{
		infraredOn = 1;
		lcd_clearDisplay();
		lcd_writeString("Infrared");
		lcd_writeStringSecond("On");
	}
	else if(infraredOn == 1)
	{
		infraredOn = 0;
		lcd_clearDisplay();
		lcd_writeString("Infrared");
		lcd_writeStringSecond("Off");
	}
}

void infrared_Pressed_Ok(void)
{
	flash_writeInteger(&infraredOn);
	navigation_HandleKeypress(KEY_ESC);
	infraredChanged = 1;
}

int getInfraredChanged(void)
{
	return infraredChanged;
}

void resetInfrared(void)
{
	infraredChanged = 0;
}

