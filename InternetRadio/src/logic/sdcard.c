#define LOG_MODULE  LOG_MAIN_MODULE

#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/thread.h>

#include "logic/sdcard.h"
#include "hardware/mmc.h"
#include "logic/log.h"
#include "hardware/portio.h"
#include "logic/audio.h"
#include "hardware/spidrv.h"

#define READ_SIZE 8192
void sd_cardInserted(void);
THREAD(sd_playFile, fid);

/**
 * Initializes the SD module. 
 */
void sd_init(void)
{
    CardInit();
    CardAddCardInsertedCallback(sd_cardInserted);
}

/**
 * Checks if a card is present. 
 * This function should be called from an interrupt routine. 
 */
void sd_checkForCard(void)
{
    CardCheckCard();
}

/**
 * Returns whether a card is inserted in the mmc slot. 
 * To get an accurate result from this function sd_checkForCard
 * should be called from the main loop interrupt routine. 
 */
bool sd_isCardInserted(void)
{
    return CardCheckPresent() == CARD_IS_PRESENT;
}

/**
 * Callback that is called when a card is inserted. The callback is registered  
 * to the mmc module in the sd_init function
 */
void sd_cardInserted(void)
{
    int *fid;
    fid = malloc(sizeof(int));

    *fid = _open("FM0:music/test.txt", _O_RDONLY | _O_BINARY);
    if (fid == -1) 
    {
        puts("failed");
    } 
    else
    {
        puts("OK");
        NutThreadCreate("sdreadingthread", sd_playFile, fid, 2048);
    }
}

THREAD(sd_playFile, fid)
{
    int fd;
    uint8_t *mp3buff;
    FILE *stream;

    fd = *((int *) fid);
    mp3buff = malloc(READ_SIZE);

    // stream = _fdopen(fd, "r+b");
    // audio_playMp3Stream(stream);

    for(;;)
    {
        int got;
        uint8_t *bp;
        FILE *stream;
        bp = mp3buff;
        got = _read(fd, bp, READ_SIZE);
        if (got <= 0) {
            break;
        }
        audio_playTest();
        //audio_playMp3Stream(bp);
        while (got) {
            int sent;
            sent = 64;
            LogMsg_P(LOG_DEBUG, PSTR("%s"), bp);
            if (sent < 0) {
                break;
            }
            if (sent == 0) {
                NutSleep(1);
            } else {
                got -= sent;
                bp += sent;
            }
        }
        break;
    }
    free(fid);
    NutThreadExit();
}
