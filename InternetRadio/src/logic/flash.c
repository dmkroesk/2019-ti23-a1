/*
 * flash.c
 *
 * Created: 26/02/2019 12:36:41
 *  Author: Tim Herreijgers
 */ 
#define LOG_MODULE  LOG_MAIN_MODULE
#include "logic/flash.h"
#include "hardware/at45db.h"
#include "logic/alarmLogic.h"
#include "logic/log.h"
#include "logic/audio.h"

#define SETTINGS_SIZE sizeof(Settings_t)
#define PERIODIC_ALARM_SIZE sizeof(PeriodicAlarm_t)
#define ALARM_TABLE_SIZE sizeof(AlarmTable_t)
#define MAC_ADDRESS_DATA_SIZE sizeof(char[13])
#define INFRARED_DATA_SIZE sizeof(int)
#define FAVORITES_SIZE sizeof(int)
#define AUDIO_SETTINGS_SIZE sizeof(AudioSettigns_t)

void flash_write(u_long page, void *data, u_int length);
void flash_read(u_long page, void *data, u_int length);

int flash_init(void)
{
    return At45dbInit();
}

void flash_write(u_long page, void *data, u_int length)
{
    At45dbPageWrite(page, data, length);
}

void flash_read(u_long page, void *data, u_int length)
{
    At45dbPageRead(page, data, length);
}

void flash_writeString(u_long page, char *string, u_int length)
{
    flash_write(page, string, length);
}

char *flash_readString(u_long page, u_int length)
{
    char *string = malloc(sizeof(char) * length);
    flash_read(page, string, length);
    return string;
}

void flash_writeSettings(u_long page, Settings_t *settings)
{
    flash_write(page, settings, SETTINGS_SIZE);
}

Settings_t *flash_readSettings(u_long page)
{
    Settings_t *settings = malloc(SETTINGS_SIZE);
    flash_read(page, settings, SETTINGS_SIZE);
    return settings;
}

void flash_writePeriodicAlarm(u_long page, PeriodicAlarm_t *alarm)
{
	flash_write(page, alarm, PERIODIC_ALARM_SIZE);
}

PeriodicAlarm_t *flash_readPeriodicAlarm(u_long page)
{
	PeriodicAlarm_t *periodicAlarm = malloc(PERIODIC_ALARM_SIZE);
	flash_read(page, periodicAlarm, PERIODIC_ALARM_SIZE);
	return periodicAlarm;
}

AlarmTable_t *flash_readAlarmTable()
{
    AlarmTable_t *alarmTable = malloc(ALARM_TABLE_SIZE);
	flash_read(ALARM_TABLE_DATA_PAGE, alarmTable, PERIODIC_ALARM_SIZE);
	return alarmTable;
}

void flash_writeAlarmTable(AlarmTable_t *alarmTable)
{
    flash_write(ALARM_TABLE_DATA_PAGE, alarmTable, ALARM_TABLE_SIZE);
}

void flash_writeMACAddress(char* mac_address)
{
    flash_write(MAC_ADDRESS_DATA_PAGE, mac_address, MAC_ADDRESS_DATA_SIZE);
}

char* flash_readMacAddress()
{
	char* MACAddress = malloc(MAC_ADDRESS_DATA_SIZE);
	flash_read(MAC_ADDRESS_DATA_PAGE, MACAddress, MAC_ADDRESS_DATA_SIZE);
	return MACAddress;
}

void flash_writeInteger(int on)
{
    At45dbPageWrite(INFRARED_DATA_PAGE, on, INFRARED_DATA_SIZE);
}

void flash_readInteger(int *on)
{
	At45dbPageRead(INFRARED_DATA_PAGE, on, INFRARED_DATA_SIZE);
}

void flash_writeFavorite(int favorite, int number)
{
	flash_write(favorite + 8, number, FAVORITES_SIZE);
}

int flash_readFavorite(int favorite)
{
	int fav = malloc(FAVORITES_SIZE);
	flash_read(favorite + 8, fav, FAVORITES_SIZE);
	return fav;
}

AudioSettigns_t* flash_readAudioSettings()
{
	AudioSettigns_t* settings = malloc(AUDIO_SETTINGS_SIZE);
	flash_read(AUDIO_SETTINGS_PAGE, settings, AUDIO_SETTINGS_SIZE);
	return settings;
}

void flash_writeAudioSettings(AudioSettigns_t *settings)
{
	flash_write(AUDIO_SETTINGS_PAGE, settings, AUDIO_SETTINGS_SIZE);	
}
