#define LOG_MODULE  LOG_MAIN_MODULE

#include <stdlib.h>
#include <fs/typedefs.h>
#include <sys/heap.h>
#include <sys/event.h>
#include <sys/atom.h>
#include <sys/types.h>
#include <dev/irqreg.h>

#include "hardware/system.h"
#include "hardware/infrared.h"
#include "hardware/lcd.h"
#include "hardware/keyboard.h"
#include "logic/log.h"

#define RC_INT_SENS_MASK      0x03
#define RC_INT_FALLING_EDGE   0x02
#define RC_INT_RISING_EDGE    0x03

#define FIRSTMESSAGE_MIN_LENGTH		2900
#define FIRSTMESSAGE_MAX_LENGTH		3300
#define BIT_HIGH_MIN_LENGTH			400
#define BIT_HIGH_MAX_LENGTH			600
#define BIT_LOW_MIN_LENGTH			200
#define BIT_LOW_MAX_LENGTH			300

static void IRInterrupt(void*);
static void IRClearEvent(HANDLE*);
void decodeData(int);

static HANDLE  hRCEvent;

uint16_t prevTimerValue = 0;

int started	   = 0;
int bitcount   = 1;
int code	   = 0;
int finalCode  = -1;

int lastButton = -1;
int sentNewKey = 0;

/*
 *  ISR Remote Control Interrupt (ISR called by Nut/OS)
 *
 *  NEC-code consists of 5 parts:
 *
 *  - leader (9 msec high, 4,5 msec low)
 *  - address (8 bits)
 *  - inverted address (8 bits)
 *  - data (8 bits)
 *  - inverted data (8 bits)
 *
 *  param *p not used (might be used to pass parms from the ISR)
 */
static void IRInterrupt(void *p)
{
	//TCNT1 is a 16 bit timer
    uint16_t timer = TCNT1;
	
	//Time since last interrupt
    uint16_t timeBetweenLast = timer - prevTimerValue;  
	
	//Checken of het het eerste signaal is
    if(!started) 
	{
        if(timeBetweenLast > FIRSTMESSAGE_MIN_LENGTH && timeBetweenLast < FIRSTMESSAGE_MAX_LENGTH) 
		{
            started = 1; 
        }
    } 
	else
	{
		//Checken ofdat het een high signal is
        if(timeBetweenLast > BIT_HIGH_MIN_LENGTH && timeBetweenLast < BIT_HIGH_MAX_LENGTH) 
		{
            decodeData(1); 
        } 
		//Checken ofdat het een low signal is
		else if(timeBetweenLast > BIT_LOW_MIN_LENGTH && timeBetweenLast < BIT_LOW_MAX_LENGTH) 
		{	
            decodeData(0); 
        } 
    }
	
    prevTimerValue = timer;
}

/*!
 * Method: Getting the 8 bit command.
 *
 * Parameter 1: bitData, is an integer with a value 
 */
void decodeData(int bitData) 
{
    if(bitcount > 16 && bitcount < 25) 
	{
        if(bitData) 
		{
            code += (1 << (24 - bitcount)); 
        }
    } 
	else if(bitcount >= 32) 
	{ 
        started = 0;
        bitcount = 0;
        finalCode = code;
        code = 0;
		codeToButton(finalCode);
    }
    
	bitcount++;
}

/*!
 * Method: Checks if the parameter code is equal to one of the cases, 
 * and than changes the variable lastButton to one of the buttons, 
 * that matches the code of the parameter code.
 * Parameter 1: code is an integer with a value between -1 and 255.
 */
void codeToButton(int code)
{
	switch (code)
	{
		case 106:					//Pressed number 1
			lastButton =  KEY_01;
			sentNewKey = 1;
		break;		
		case 136:					//Pressed number 2
			lastButton =  KEY_02;
			sentNewKey = 1;
		break;
		case 72:					//Pressed number 3
			lastButton =  KEY_03;
			sentNewKey = 1;
		break;		
		case 200:					//Pressed number 4
			lastButton =  KEY_04;
			sentNewKey = 1;
		break;		
		case 40:					//Pressed number 5
			lastButton =  KEY_05;
			sentNewKey = 1;
		break;	
		case 80:					//Pressed alt
			lastButton =  KEY_ALT;
			sentNewKey = 1;
		break;		
		case 255:					//Pressed esc
			lastButton =  KEY_ESC;
			sentNewKey = 1;
		break;		
		case 0:						//Pressed up
			lastButton =  KEY_UP;
			sentNewKey = 1;
		break;		
		case 88:					//Pressed ok
			lastButton =  KEY_OK;
			sentNewKey = 1;
		break;		
		case 192:					//Pressed left
			lastButton =  KEY_LEFT;
			sentNewKey = 1;
		break;		
		case 128:					//Pressed down
			lastButton =  KEY_DOWN;
			sentNewKey = 1;
		break;		
		case 64:					//Pressed right
			lastButton =  KEY_RIGHT;
			sentNewKey = 1;
		break;
	}
}

/*
 * Method: Clears the eventbuffer of this module
 *
 * This routine is called during module initialization.
 *
 * Parameter 1: *pEvent, pointer to the event queue
 */
static void IRClearEvent(HANDLE *pEvent)
{
    NutEnterCritical();
    *pEvent = 0;
    NutExitCritical();
}

void IRInit()
{
    int nError = 0;

    EICRB &= ~RC_INT_SENS_MASK;  

    // Install Remote Control interrupt
    nError = NutRegisterIrqHandler(&sig_INTERRUPT4, IRInterrupt, NULL);
	
    if (nError == FALSE)
    {
        EICRB |= RC_INT_FALLING_EDGE;
        EIMSK |= 1<<IRQ_INT4;       
    } 
	else 
	{
        LogMsg_P(LOG_ALERT, PSTR("infrared.c, IRInit(), Can't register Irq handler"));
    }

    // Initialise 16-bit Timer (Timer1)
    TCCR1B |= (1<<CS11) | (1<<CS10); // clockdivider = 64
    TIFR   |= 1<<ICF1;

    IRClearEvent(&hRCEvent);
}

int	getSentNewKey(void)
{
	return sentNewKey;
}

int	getLastButton(void)
{
	return lastButton;
}

void resetSentNewKey(void)
{
	sentNewKey = 0;
}