
///
#define LOG_MODULE  LOG_MAIN_MODULE

#include <sys/thread.h>
#include <sys/timer.h>

#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>


#include "logic/log.h"
#include "hardware/ethernet.h"

#include <sys/socket.h>
#include "logic/jsmn.h"

#include <stdio.h>
#include <string.h>

#define ETH0_BASE	0xC300
#define ETH0_IRQ	5

#define OK			0
#define NOK			-1

#define RADIO_IPADDRESS	"50.7.98.106"   // Radiowax.com
#define RADIO_PORT		8465				// Radiowax.com
#define RADIO_URL		"/"				// Radiowax.com

static char eth0IfName[9] = "eth0";
FILE *stream;
TCPSOCKET *sock;


char buff[2048];
char tempfromjson[20];
char *ret;


THREAD(EthernetThread, args) {
	RadioStation_t* station = args;
	char* ip = station->ip;
	int port = station->port;
	int name = station->name;
	
	int result = OK;
	char *data;
	
	LogMsg_P(LOG_ERR, PSTR("Connecting to stream......."));
	sock = NutTcpCreateSocket();
	int i = 0;
	int errorCode;
	for(; i < 50; i++) {
		if(NutTcpConnect(sock, inet_addr(ip), port))
		{
			errorCode = NutTcpError(sock);
			LogMsg_P(LOG_ERR, PSTR("Error: >> NutTcpConnect() %d"), errorCode);
		}
		else {
			break;
		}
		NutSleep(100);
	}
	stream = _fdopen( (int) sock, "r+b" );
	fprintf(stream, "GET %s HTTP/1.0\r\n", RADIO_URL);
	//fprintf(stream, "Host: %s\r\n", "62.212.132.54");
	fprintf(stream, "Host: %s\r\n", inet_addr(RADIO_IPADDRESS));
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
	fprintf(stream, "Icy-MetaData: 1\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	audio_playStream(stream);
	fflush(stream);
	
	// Server stuurt nu HTTP header terug, catch in buffer
	data = (char *) malloc(512 * sizeof(char));
	
	while( fgets(data, 512, stream) )
	{
		if( 0 == *data )
			break;

		printf("%s", data);
	}
	
	printf("stream linked");
	free(data);		
	NutThreadExit();
}

void ethernet_init(void)
{
	uint8_t mac_addr[6] = {0x00, 0x50, 0x56, 0x00, 0x00, 0x38};
	
	int result = OK;

	// Registreer NIC device (located in nictrl.h)
	result = NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ);
	LogMsg_P(LOG_DEBUG, PSTR("NutRegisterDevice() returned %d"), result);
	if(result)
	{
		LogMsg_P(LOG_ERR, PSTR("Error: >> NutRegisterDevice() %d"));
		result = NOK;
	}
	
	if( OK == result )
	{
		LogMsg_P(LOG_INFO, PSTR("Getting IP address (DHCP)"));
		if( NutDhcpIfConfig(eth0IfName, NULL, 10000) )
		{
			LogMsg_P(LOG_ERR, PSTR("Error: no stored MAC address, try hardcoded MAC"));
			if( NutDhcpIfConfig(eth0IfName, mac_addr, 0) )
			{
				LogMsg_P(LOG_ERR, PSTR("Error: >> NutDhcpIfConfig()"));
				result = NOK;
			}
		}
	}
	
	if( OK == result )
	{
		LogMsg_P(LOG_INFO, PSTR("Networking setup OK, new settings are:\n") );

		LogMsg_P(LOG_INFO, PSTR("if_name: %s"), confnet.cd_name);
		LogMsg_P(LOG_INFO, PSTR("ip-addr: %s"), inet_ntoa(confnet.cdn_ip_addr) );
		LogMsg_P(LOG_INFO, PSTR("ip-mask: %s"), inet_ntoa(confnet.cdn_ip_mask) );
		LogMsg_P(LOG_INFO, PSTR("gw     : %s"), inet_ntoa(confnet.cdn_gateway) );
	}
	
	NutSleep(1000);
	
}


void ethernet_connectStream(RadioStation_t *radio) 
{
	NutThreadCreate("EthernetThread",EthernetThread,radio, 2048);	
}

void ethernet_closeStream(void)
{
	NutTcpCloseSocket(sock);
	LogMsg_P(LOG_INFO, PSTR("Closed socket"));	
	//LogMsg_P(LOG_INFO, PSTR("FClosed stream FCLOSE"));
	audio_stopStream();
}

/*
 * Checks internet availability 
 *
 *
 * return 0 if true or -1 if false 
 */
int ethernet_checkconnection(void){
    TCPSOCKET *sock;
    sock = NutTcpCreateSocket();

    if( NutTcpConnect(sock, inet_addr("84.24.29.115"),80) ) {
        return 0;
    } else {
        return 1;
    }
}

int ethernet_getRequest(char* ip, int port, char* route, char* host) {  
    int timeOutValue = 15000;
	int segmentation = 1460;
	int returnBuffer = 8192;


    sock = NutTcpCreateSocket();
    NutTcpSetSockOpt(sock, 0x02, &segmentation, sizeof(segmentation));

	NutTcpSetSockOpt(sock, SO_RCVTIMEO, &timeOutValue, sizeof(timeOutValue));	

	NutTcpSetSockOpt(sock, SO_RCVBUF, &returnBuffer, sizeof(returnBuffer));
	
   	NutTcpConnect(sock, inet_addr(ip), port);

	stream = _fdopen((int) sock, "r+b");

	fprintf(stream, "GET %s HTTP/1.0\r\n", route);
    fprintf(stream, "Host: %s\r\n", host);
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
    fprintf(stream, "Connection: close\r\n\r\n");
    fflush(stream);


    while (fgets(buff, sizeof(buff), stream)) {
        puts(buff);
    }

    fclose(stream);
    NutTcpCloseSocket(sock);
    
	return 1;
}

void getTitle(char *newsTitle) {

	char title[8] = "title";
	char* data;

	data = strstr(buff, title);
	int i =6;

	while(data[i] != ',') {
		newsTitle[i-6] = data[i];
		i++;
	}

	newsTitle[i-6] = '\0';
}
