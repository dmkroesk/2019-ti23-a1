#ifndef TIMER_H_
#define TIMER_H_

#include <sys/timer.h>

/**
 * Typedef of the timer handle type.
 */
typedef HANDLE handle_t;

/**
 * Typedef of the timer callback type. This callback has the following prototype:
 * void function(handle_t timer, void *args)
 */
typedef void(*timerCallback_t)(HANDLE handle, void *args);

/*
 * Timer start method that uses the NutOS timers for holding time. 
 * Even after the timer elapsed, the callback function is not executed before the currently running thread 
 * is ready to give up the CPU. Thus, system timers may not fulfill the required accuracy.
 *
 * @param msec 	The amount of seconds before the callback is called
 * @param callback The callback
 * @param args 	The arguments that the callback should be called with
*/
void *timer_callAfter(int msec, timerCallback_t callback, void *args);

/**
 * Stops an currently running timer
 * 
 * @Param handle The timer handle
 */
void timer_stop(handle_t handle);

#endif /* TIMER_H_ */
