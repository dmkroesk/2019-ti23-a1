/*
 * flash.h
 *
 * Created: 26/02/2019 12:36:54
 *  Author: Tim Herreijgers
 */ 


#ifndef FLASH_H_
#define FLASH_H_

#define TEST_DATA_PAGE		  0
#define SETTINGS_DATA_PAGE	  1

#define ALARM1_DATA_PAGE	  2
#define ALARM2_DATA_PAGE	  3
#define ALARM3_DATA_PAGE	  4
#define ALARM4_DATA_PAGE	  5
#define ALARM5_DATA_PAGE	  6

#define ALARM_TABLE_DATA_PAGE 7
#define MAC_ADDRESS_DATA_PAGE 8

#define FAVORITE1_DATA_PAGE  9
#define FAVORITE2_DATA_PAGE  10
#define FAVORITE3_DATA_PAGE  11
#define FAVORITE4_DATA_PAGE  12
#define FAVORITE5_DATA_PAGE  13

#define AUDIO_SETTINGS_PAGE  14

#define INFRARED_DATA_PAGE 15

#include <sys/types.h>
#include "logic/settings.h"
#include "logic/alarmLogic.h"
#include "logic/audio.h"

/**
 * Initializes the flash module.
 */
extern int flash_init(void);

/**
 * Writes a string to the flash. The page should be one of the predefined values
 * 
 * @param page The page
 * @param string The string
 * @param length The length in bytes
 */
extern void flash_writeString(u_long page, char *string, u_int length);

/**
 * Reads a string from the flash. The page should be one of the predefined values
 * 
 * @param page The page
 * @param length The length in bytes
 * 
 * @return The string
 */
extern char *flash_readString(u_long page, u_int length);

/**
 * Writes settings to the flash. The page should be one of the predefined values
 * 
 * @param page The page
 * @param settings The settings
 */
extern void flash_writeSettings(u_long page, Settings_t *settings);

/**
 * Reads settings from the flash. The page should be one of the predefined values
 * 
 * @param page The page
 * 
 * @return The settings
 */
extern Settings_t *flash_readSettings(u_long page);

/**
 * Writes settings to the flash. The page should be one of the predefined values
 * 
 * @param page The page
 * @param settings The settings
 */
extern void flash_writeSettings(u_long page, Settings_t *settings);

/**
 * Reads an alarm from the flash. The page should be one of the predefined values
 * 
 * @param page The page
 * 
 * @return The alarm
 */
extern PeriodicAlarm_t *flash_readPeriodicAlarm(u_long page);

/**
 * Writes an alarm to the flash. The page should be one of the predefined values
 * 
 * @param page The page
 * @param alarm The alarm
 */
extern void flash_writePeriodicAlarm(u_long page, PeriodicAlarm_t *alarm);

/**
 * Reads an alarmTable from the flash. The page should be one of the predefined values
 * 
 * @param page The page
 * 
 * @return The alarmTable
 */
extern AlarmTable_t *flash_readAlarmTable( void);


extern void flash_writeAlarmTable(AlarmTable_t *alarmTable);

/*
 * writes an MAC_Address to the flash
 *
 * param char*
 *
 * return void
 *
 */
extern void flash_writeMACAddress(char* mac_address);

/*
 * read an macaddress from the flash
 *
 * param
 *
 * return char*
 *
 */
extern char* flash_readMacAddress(void);

void flash_write(u_long page, void *data, u_int length);

void flash_writeFavorite(int, int);
int flash_readFavorite(int);

/*
 * read the audio settings from the flash
 *
 * param
 *
 * return *
 *
 */
extern AudioSettigns_t* flash_readAudioSettings();

void flash_writeAudioSettings(AudioSettigns_t*);
extern void flash_writeInteger(int);

extern void flash_readInteger(int*);

#endif /* FLASH_H_ */
