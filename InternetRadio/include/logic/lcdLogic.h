/*
 * lcdLogic.h
 *
 * Created: 21-2-2019 10:28:32
 *  Author: Ralph Rouwen
 */ 


#ifndef LCDLOGIC_H_
#define LCDLOGIC_H_

void lcd_init(void);
void lcd_custom_internet(void); 
void lcd_writeString(char *string);
void lcd_clearDisplay(void);
void lcd_backlightOff(void);
void lcd_backlightOn(void);
void lcd_writeStringSecond(char *string);
void lcd_custom_internet(void);
void LcdString(char string[]);
void scroll_threadCreate(void);
void scrollThroughText(char* text);



#endif /* LCDLOGIC_H_ */
