/*
 * audio.h
 *
 * Created: 12/03/2019 10:30:40
 *  Author: Tim Herreijgers
 */ 


#ifndef AUDIO_H_
#define AUDIO_H_

typedef struct{
	u_int leftVol;
	u_int rightVol;
	u_int bass;
	u_int treble;
}AudioSettigns_t;

void audio_init(void);
void audio_playTest(void);
void audio_playStream(void *);
void audio_playMp3Stream(void *);
void audio_stopPlayback(void);
int  audio_getLeftVol(void);
int  audio_getRightVol(void);
void audio_volUp(void);
void audio_volDown(void);
int  audio_getBass(void);
void audio_bassUp(void);
void audio_bassDown(void);
int	 audio_getTreble(void);
void audio_trebleUp(void);
void audio_trebleDown(void);
void audio_stopStream(void);

#endif /* AUDIO_H_ */

