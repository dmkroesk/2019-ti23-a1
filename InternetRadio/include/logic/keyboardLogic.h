/*
 * KeyboardLogic.h
 *
 * Created: 21-2-2019 09:51:27
 *  Author: olaf
 */ 

#ifndef KEYBOARDLOGIC_H
#define KEYBOARDLOGIC_H

typedef void(*keyCallback_t)(int);

void kbl_addCallback(keyCallback_t callback);
void kbl_update(void);
void kbl_init(void);

void updateSelectedItem(void);
void pressed_Ok(void);
void pressed_Esc(void);
void pressed_Up(void);
void pressed_Down(void);
void pressed_Left(void);
void pressed_Right(void);
void pressed_1(void);
void pressed_2(void);
void pressed_3(void);
void pressed_4(void);
void pressed_5(void);
int  isLeft(int);
int  isRight(int);
void pressed_power(void);
void infrared_Pressed(int);

void pressed_next(void);
void pressed_prev(void);
void pressed_increment(void);
void pressed_decrement(void);
void pressed_time_accept(void);

void pressed_next_alarm(void);
void pressed_prev_alarm(void);
void pressed_increment_alarm(void);
void pressed_decrement_alarm(void);
void pressed_accept_alarm(void);
void resetVariables(void);

void pressed_increment_alarmTime(void);
void pressed_decrement_alarmTime(void);
void pressed_alarmstop_accept(void);


void getAlarms(void);
void kbl_keyPressedDownAlarmDelete(void);
void kbl_keyPressedUpAlarmDelete(void);
void kbl_showAlarms(int change);
void kbl_deleteAlarm(void);

void MAC_pressed_next(void);
void MAC_pressed_prev(void);
void MAC_increment(void);
void MAC_decrement(void);
void MAC_pressed_ok(void);
void favorites_pressed_right(void);
void favorites_pressed_left(void);
void infrared_Pressed_Left_Or_Right(void);
void infrared_Pressed_Ok(void);
int getInfraredChanged(void);
void resetInfrared(void);


void pressed_bass_Down(void);
void pressed_bass_Up(void);
void pressed_treble_Down(void);
void pressed_treble_Up(void);
void pressed_audio_Up(void);
void pressed_audio_Down(void);
void pressed_bass_Ok(void);
void pressed_treble_Ok(void);
#endif
