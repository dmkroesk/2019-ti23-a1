#include <stdbool.h>

extern void sd_init(void);
extern void sd_checkForCard(void);
extern bool sd_isCardInserted(void);
// void sd_printContents(void);