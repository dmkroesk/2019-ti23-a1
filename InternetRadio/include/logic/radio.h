/*
 * radio.h
 *
 * Created: 14-3-2019 12:36:49
 *  Author: olaf
 */ 


#ifndef RADIO_H_
#define RADIO_H_

#define RADIO_STATION_LIST_SIZE 5

typedef struct 
{
	char* name;
	char* ip;
	int port;
} RadioStation_t;

extern RadioStation_t radioStationList[RADIO_STATION_LIST_SIZE];

#endif
