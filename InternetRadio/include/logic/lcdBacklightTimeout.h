#ifndef _LcdBacklightTimeout_H
#define _LcdBacklightTimeout_H

/**
 * Initializes the lcdBacklightTimeout module. By default the module will be enabled when it is
 * initialized. 
 */
void lbt_init(void);

/**
 * Disables the lcdBacklightTimeout module.
 */
void lbt_disable(void);

/**
 * Enables the lcdBacklightTimeout module. By default the module is enabled when it is initialized
 */
void lbt_enable(void);

#endif //#endif _LcdBacklightTimeout_H

