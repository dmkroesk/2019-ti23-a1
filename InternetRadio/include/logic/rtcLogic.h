/*
 * rtcLogic.h
 *
 * Created: 19-2-2019 16:16:45
 *  Author: Ralph Rouwen
 */ 


#ifndef RTCLOGIC_H_
#define RTCLOGIC_H_

#include <stdbool.h> 

struct _tm;
void rtc_setTime(int sec, int min, int hour, int day, int month, int year);
int rtc_getTime(struct _tm *time);
int rtc_init(void);
void rtc_getTimeAsString(char *time);
void rtc_parseHoursAndMinutesToString(int hours, int min, char *time);
void rtc_parseTimeStampHrMinToString(int hours, int min, char *timestamp);
int getMinutes(void);
void rtc_parseTimeStampToString(int hours, int min, int days, int month, int year, char *timestamp);
void rtc_update(void);
/* End of prototypes */

/**
 * Checks if the provided date is valid. It will check if the day excists in the current month. 
 * Leap years will be taken into account. This function will return 0 if the value is not valid and 
 * any other value when it is. 
 * 
 * @param day The day
 * @param month The month
 * @param year The year
 * 
 * @return Whether the date is valid
 */
bool rtc_checkValidDate(int day, int month, int year);

#endif /* RTCLOGIC_H_ */
