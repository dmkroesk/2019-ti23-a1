/*
 * alarmLogic.h
 *
 * Created: 26-2-2019 12:12:30
 *  Author: Olaf
 */ 


#ifndef ALARMLOGIC_H_
#define ALARMLOGIC_H_

#include <stdbool.h>

typedef struct PeriodicAlarm{
	int hours, minutes;
	int alarmAudioType;
	int turnedOn;
	int timesSnoozed;
} PeriodicAlarm_t;

typedef struct PlannedAlarm{
	int hours, minutes;
	int alarmAudioType;
	int days[7];
	int turnedOn;
} PlannedAlarm_t;

typedef struct AlarmTable 
{
	bool alarm1Active;
	bool alarm2Active;
	bool alarm3Active;
	bool alarm4Active;
	bool alarm5Active;
} AlarmTable_t;

void alarm_init(void);
void alarm_toggle(int);
void alarm_setTime(int,int,int);
void alarm_setAudioType(int,int);
void alarm_init(void);
void alarm_addNewAlarm(int hours, int minutes, int audioType, int turnedOn);
PeriodicAlarm_t alarm_getPeriodicAlarmFromMem(int page);
void alarm_getAlarmList(PeriodicAlarm_t *alarms[5]);
void alarm_savePeriodicAlarmGlobally(void);
void alarm_update(void);

extern PeriodicAlarm_t *alarm1;
extern PeriodicAlarm_t *alarm2;
extern PeriodicAlarm_t *alarm3;
extern PeriodicAlarm_t *alarm4;
extern PeriodicAlarm_t *alarm5;

#endif /* ALARMLOGIC_H_ */
