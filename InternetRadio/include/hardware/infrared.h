
/*
 * infrared.h
 *
 *	VIVANCO UR 89 Universal Controller: code 057
 *	Usage of NEC protocol
 *
 *  Created: 19-3-2019 10:58:10
 *  Version: 5.0
 *  Last updated: 11:30, 22-03-2019
 *
 *  Author: Damian Visser
 *  Studentcode: 2125509 
*/

#ifndef INFRARED_H_
#define INFRARED_H_

/*
 * Method to initialise the Remote Control module.
 * Used in Main.c, to initialise the IR sensor, 
 * the timer and the external interrupt used to decode the signals send from the remote.
 *
 * 1. Register the ISR in NutOS
 * 2. Initialise the HW-timer that is used for this module (Timer1)
 * 3. Initialise the external interrupt that inputs the infrared data
 * 4. Flush the remote control buffer
 * 5. Flush the eventqueue for this module
 */
void IRInit(void);

/*
 * Method to get the value of sentNewKey.
 * Used in Main.c.
 * Used variables: sentNewKey, used to see if there is a new button pressed on the remote.
 */
int	getSentNewKey(void);

/*
 * Method to get the last pressed button.
 * Used in Main.c, if the sentNewKey variable is 1 (true).
 * Used variables: lastButton, returns the value of lastButton.
 */
int	getLastButton(void);

/*
 * Method to reset the sentNewKey variable.
 * Used in Main.c, after using the lastButton variable.
 * Used variables: sentNewKey, is set to 0 (false).
 */
void resetSentNewKey(void);


#endif /* INFRARED_H_ */

