/*
 * ethernet.h
 *
 * Created: 28-2-2019 10:31:06
 *  Author: User
 */ 
#include "logic/radio.h"

void ethernet_init(void);
void ethernet_connectStream(RadioStation_t *);
int  ethernet_checkconnection(void);
void ethernet_closeStream(void);
int ethernet_getRequest(char* ip, int port, char* route, char* host);
void getTitle(char *newsTitle);
void getTemp(char *weatherTemp);