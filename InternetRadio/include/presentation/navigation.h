/*
 * navigation.h
 *
 * Created: 19-2-2019 12:10:39
 *  Author: dvisser4
 */ 
#ifndef NAVIGATION_H

#define NAVIGATION_H

void navigation_HandleKeypress(int);
void navigation_initMenu(void);

int currentSelectedItem;
int currentItem;
int currentId;

#endif
