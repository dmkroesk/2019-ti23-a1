/*
 * menu.h
 *
 * Created: 19-2-2019 11:57:44
 *  Author: dvisser4
 */ 
#ifndef MENU_H

#define MENU_H
#define MAX_KEYS							15
#define MAX_ITEMS							5
#define LDC_MAX_CHARS						32

#define CURRENT_MENU						0
#define MENU_MAIN							100
#define MENU_MENU							101

#define MENU_SETTINGS						102 // vanaf MENU_MENU

#define MENU_RADIO							103 // vanaf MENU_MENU
#define MENU_FAVORITES						104 // vanaf MENU_MENU
#define MENU_AUDIO							105 // vanaf MENU_MENU

#define MENU_ALARM							200 // vanaf MENU_MENU
#define MENU_ALARM_ADD						202
#define MENU_ALARM_DELETE					203

#define MENU_ALARM_CHANGE					210 //vanaf MENU_ALARM
#define MENU_ALARM_CHANGE_SOUND				211
#define MENU_ALARM_CHANGE_TIME				212
#define MENU_ALARM_CHANGE_DAY				213

#define MENU_SETTINGS_TIME					300 //vanaf MENU_INSTELLINGEN
#define MENU_SETTINGS_STOPALARMTIME			301
#define MENU_SETTINGS_MAC					302
#define MENU_SETTINGS_INFRARED				369

#define MENU_AUDIO_BASS						501
#define MENU_AUDIO_TREBLE					502

typedef struct
{
	unsigned int id;
	unsigned int nextId[MAX_KEYS];
	const char* selectedItem[MAX_ITEMS];
	unsigned int selectedItemId[MAX_ITEMS];
		
	void(*OnKey[MAX_KEYS])(void);
	void(*OnEntry)(void);
	void(*OnExit)(void);
} MenuItem_t;

extern MenuItem_t menu[];
extern void startItem(int, int);

#endif
